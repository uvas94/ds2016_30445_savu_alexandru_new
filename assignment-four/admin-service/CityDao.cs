﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace admin_service
{
    using AssigfourContext;
    class CityDao
    {
        public List<City> getAllCities()
        {
            AssigfourDataContext context = new AssigfourDataContext();

            List<City> cities = new List<City>();
            var query = from it in context.City
                        select it;
            try
            {
                foreach (City city in query)
                    cities.Add(city);
            }
            catch(Exception e)
            {
                Console.WriteLine("Error: " + e);
            }
            return cities;
        }
        public City getCityByName(String name)
        {
            AssigfourDataContext context = new AssigfourDataContext();

            var query = from it in context.City
                        where it.cityname == name
                        select it;
            City city = null;
            foreach (City city1 in query)
                city = city1;
            return city;
        }
    }
}
