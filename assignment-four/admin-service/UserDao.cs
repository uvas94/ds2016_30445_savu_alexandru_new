﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace admin_service
{

    using AssigfourContext;
    class UserDao
    {

        public List<User> getAllUsers()
        {
            AssigfourDataContext context = new AssigfourDataContext();

            List<User> users = new List<User>();
            var query = from it in context.User
                        select it;
            try
            {
                foreach (User user in query)
                    users.Add(user);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
            }
            return users;
        }
    }

}
