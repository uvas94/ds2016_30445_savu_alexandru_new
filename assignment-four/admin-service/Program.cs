﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace admin_service
{
    using AssigfourContext;
   
        class Program
        {
        

            static void Main(string[] args)
        {
           /* List<Pair<city,DateTime>> list = new List<Pair<city,DateTime>>();
            for(int i = 0; i <= 3; i++){
            city _city = new city();
            _city.cityName = "Cluj";
            Pair<city,DateTime> pair = new Pair<city,DateTime>();
            pair.First = _city;
            pair.Second = new DateTime();
            list.Add(pair);
            }
            string json = JsonConvert.SerializeObject(list);
            Console.WriteLine(json);
            
            PacketDao packetDao = new PacketDao();
            Packet pck = new Packet();
            City _city1 = new City();
            City _city2 = new City();
            _city1.idcity = 5;
            _city1.cityname = "Bratislava";
            _city1.lat = 46.7667;
            _city1.@long = 23.623635;
            _city2.idcity = 3;
            _city2.cityname = "Bucuresti";
            _city2.lat = 44.426767;
            _city2.@long = 26.102538;
            Route route = new Route();
            route.idroute = 2;
            route.destinationcity = _city2.idcity;
            route.sendercity = _city1.idcity;
            route.currentcity = _city1.idcity;  
            //route.City_currentcity = _city1;
            //route.City_destinationcity = _city2;
            //route.City_sendercity = _city1;

            User user1 = new User();
            User user2 = new User();
            user1.iduser = 1;
            user2.iduser = 2;
            pck.sender = user1.iduser;
            pck.reciver = user2.iduser;
            pck.route = route.idroute;
            pck.tracking = 0;
            pck.description = "sC####";
            pck.name = "c#####";
            pck.Route_route = route;
            
            Packet pakcett = packetDao.save(pck);
            Console.WriteLine(pakcett.name); */

            Uri baseAddress = new Uri("http://localhost:8082/admin-service/adminService");
            //BasicHttpBinding binding1 = new BasicHttpBinding();
            using (ServiceHost host = new ServiceHost(typeof(WebServer), baseAddress))
            {

                //host.AddServiceEndpoint(typeof(IWebServer), binding1, baseAddress);
                // Enable metadata publishing.
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
                host.Description.Behaviors.Add(smb);
               
                //host.ManualFlowControlLimit = 100000000;
                // Open the ServiceHost to start listening for messages. Since
                // no endpoints are explicitly configured, the runtime will create
                // one endpoint per base address for each service contract implemented
                // by the service.
                host.Open();

                Console.WriteLine("The service is ready at {0}", baseAddress);
                Console.WriteLine("Press <Enter> to stop the service.");
                Console.ReadLine();

                // Close the ServiceHost.
                host.Close();
            }
            
            
            
        }

        }
    
}
