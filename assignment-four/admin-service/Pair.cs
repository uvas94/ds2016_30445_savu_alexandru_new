﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace admin_service
{
    [DataContract(Name = "Pair")]
    public class Pair<T,U> : IExtensibleDataObject
    {
        public Pair() { }
        public Pair(T first, U second)
        {
            this.First = first;
            this.Second = second;
        }
         [DataMember(Name = "first", IsRequired = true)]
        public T First { get; set; }
         [DataMember(Name = "second", IsRequired = true)]
        public U Second {get;set;}

         public ExtensionDataObject ExtensionData{get;set;}
    }
}
