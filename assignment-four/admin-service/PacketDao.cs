﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace admin_service
{
    using AssigfourContext;
    
    class PacketDao
    {
       
        public Packet save(Packet packet) { 
            AssigfourDataContext context = new AssigfourDataContext();
            try
            {
                context.Packet.InsertOnSubmit(packet);
                context.SubmitChanges();
            }
            catch (Exception e) { Console.Out.WriteLine("Save-Error:"+e);}

            var entity1 = context.Packet.FirstOrDefault(twl => twl.idpacket == packet.idpacket);
            
            // Since we query for a single object instead of a collection, we can use the method First()

            if (entity1 != null)
                return entity1;
            return null;
        }

        public Packet update(Packet packet)
        {
            AssigfourDataContext context = new AssigfourDataContext();
            try
            {
                var entity = context.Packet.FirstOrDefault(twl => twl.idpacket == packet.idpacket);
                if (entity != null)
                {
                    entity.name = packet.name;
                    entity.description = packet.description;
                    entity.sender = packet.sender;
                    entity.reciver = packet.reciver;
                    entity.sendercity = packet.sendercity;
                    entity.currentcity = packet.currentcity;
                    entity.destinationcity = packet.destinationcity;
                    entity.tracking = packet.tracking;
                    entity.route = packet.route;

                    context.SubmitChanges();
                }
                
            }
            catch (Exception e) { Console.Out.WriteLine("Save-Error:" + e); }
           var entity1 = context.Packet.FirstOrDefault(twl => twl.idpacket == packet.idpacket);
               

            // Since we query for a single object instead of a collection, we can use the method First()
           
           if (entity1 != null)
               return entity1;
            return null;
        }
        
        public void delete(Packet packet){
            AssigfourDataContext context = new AssigfourDataContext();
            try
            {
                var entity1 = context.Packet.FirstOrDefault(twl => twl.idpacket == packet.idpacket);

                context.Packet.DeleteOnSubmit(entity1);
                context.SubmitChanges();
            }
            catch (Exception e) { Console.Out.WriteLine("Delete-Error:" + e); }
        }

        public List<Packet> read(){
            AssigfourDataContext context = new AssigfourDataContext();
            var query = from it in context.Packet
                        select it;
            List<Packet> packets = new List<Packet>();
            try
            {
                foreach (Packet pckt in query)
                    packets.Add(pckt);
            }
            catch(Exception e)
            { Console.WriteLine("Error:" + e); }
                return packets;
        }
        public Packet findPacketById(long idpacket)
        {
            AssigfourDataContext context = new AssigfourDataContext();
            var entity1 = context.Packet.FirstOrDefault(twl => twl.idpacket == idpacket);
            if (entity1 != null)
                return entity1;
            return null;
        }
       /* public void dbOperation(Packet packet)
        {
            string connectionString = "server=localhost;port=3306;database=assig-four;uid=root;password=root";
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                using (ContextSettings contextDB = new ContextSettings(connection, false))
                {
                    contextDB.Database.CreateIfNotExists();
                }

                connection.Open();
                MySqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (ContextSettings context = new ContextSettings(connection, false))
                    {
                        // Interception/SQL logging
                        context.Database.Log = (string message) => { Console.WriteLine(message); };

                        // Passing an existing transaction to the context
                        context.Database.UseTransaction(transaction);

                        context.Packets.Add(packet);
                        context.SaveChanges();

                      
                    }
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }*/

    }
}
