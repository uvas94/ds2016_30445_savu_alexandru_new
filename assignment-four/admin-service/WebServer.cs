﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.ServiceModel;

using System.ServiceModel.Description;

namespace admin_service
{

    using AssigfourContext;
    [ServiceContract(Name = "WebService")]
    public interface IWebServer
    {
        [OperationContract] 
        List<Packet> getAllPackets();
        [OperationContract]
        long savePacket(Packet packet);
        [OperationContract]
        void deletePacket(Packet packet);
        [OperationContract]
        void updatePacket(Packet packet);
        [OperationContract]
        List<City> getAllCities();
        [OperationContract]
        City getCityByName(String name);
        [OperationContract]
        List<User> getAllUsers();
        [OperationContract]
        void trackPacket(long idpacket, List<Pair<long, String>> route);
        [OperationContract]
        int retrunI() ;

    }
    class WebServer : IWebServer 
    {
        public PacketDao packetDao;
        public CityDao cityDao;
        public UserDao userDao;
        public WebServer()
        {
            packetDao = new PacketDao();
            cityDao = new CityDao();
            userDao = new UserDao();
        }
        public List<Packet> getAllPackets()
        {
            List<Packet> packets = packetDao.read();
            if (packets == null)
                return new List<Packet>();
            return packets;
        }

        public long savePacket(Packet packet) { 
            Packet savedPacket = packetDao.save(packet);
            if(savedPacket != null)
                return savedPacket.idpacket;
            
            return -1;
        }

        public void deletePacket(Packet packet)
        {
            packetDao.delete(packet);
        }

        public void updatePacket(Packet packet)
        {
            packetDao.update(packet);
        }

        public List<City> getAllCities()
        {
            List<City> cities = cityDao.getAllCities();
            if (cities == null)
                return new List<City>();
            return cities;
        }

        public City getCityByName(String name)
        {
            City city = cityDao.getCityByName(name);
            if (city == null)
                return new City();
            return city;
        }

        public List<User> getAllUsers()
        {
            List<User> users = userDao.getAllUsers();
            if (users == null)
                return new List<User>();
            return users;
        }

        public void trackPacket(long idpacket, List<Pair<long,String>> route)
        {
            Packet packet = packetDao.findPacketById(idpacket);
            packet.tracking = true;
            packet.route = JsonConvert.SerializeObject(route);
            updatePacket(packet);
            RouteThread rt = new RouteThread(packet,route);

            Thread th = new Thread(new ThreadStart(rt.update));
            th.Start();
        }
        public int retrunI() { return 1; }
    }
}
