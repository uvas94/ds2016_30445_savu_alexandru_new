﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace admin_service
{
    using AssigfourContext;
    public class RouteThread
    {
        public List<Pair<long,String>> pair;
        Packet packet;
        PacketDao packetDao;
        CityDao cityDao;
        public RouteThread(Packet packet, List<Pair<long, String>> pair)
        {
            this.pair = pair;
            this.packet = packet;
            this.packetDao = new PacketDao();
            this.cityDao = new CityDao();
        }

        public void update(){
            int n = 0;
            while (packet.currentcity != packet.destinationcity && n<pair.Count )
            {
                
                long idcurCity = pair.ElementAt(n).First;
                Console.WriteLine("Packet " + packet.name + " is in " + idcurCity);
                packet.currentcity = idcurCity;
                packetDao.update(packet);
                System.Threading.Thread.Sleep(20000);
                n++;
            }

        }
    }
}
