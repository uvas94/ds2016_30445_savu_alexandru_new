package org.client.service.services;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import org.client.service.model.City;
import org.client.service.model.ListWrapper;
import org.client.service.model.Packet;
import org.client.service.model.User;


@WebService(targetNamespace  = "PacketServices.services.server.web.four.assig.org")
@SOAPBinding(style =Style.RPC)
public interface PacketServices {
	
	@WebMethod public ListWrapper<Packet> getAllUserPackages(User user); 
	@WebMethod public int saveUserPacket(Packet packet);
	@WebMethod public int deleteUserPacket(Packet packet);
	@WebMethod public ListWrapper<City> getAllCities();
	@WebMethod public City getCurrentCity(Packet packet);
	@WebMethod public ListWrapper<Packet> searchPacketByName(String name,User user);
	
	@WebMethod public User findUserByUsername(String username);
	@WebMethod public int save(User user);
	//@WebMethod public ArrayList<User> getAllUsers();
}
