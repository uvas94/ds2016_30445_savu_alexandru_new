package org.client.service.services;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import org.client.service.model.User;

@WebService(targetNamespace  = "UserServices.services.server.web.four.assig.org")
@SOAPBinding(style =Style.RPC)
public interface UserServices {

	@WebMethod public User findUserByUsername(String username);
	@WebMethod public int save(User user);
}
