package org.client.service.services;

import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;

import org.client.service.dao.UserDao;
import org.client.service.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@WebService(
		serviceName = "UserService",
		targetNamespace  = "UserServices.services.server.web.four.assig.org",
		endpointInterface = "org.client.service.services.UserServices")
@XmlRootElement
public class UserServicesImpl implements UserServices{
	
	@Autowired
	private UserDao userDao;
	
	
	public User findUserByUsername(String username){
		 
		if(userDao == null)
			return new User();
		User user = userDao.findByUsername(username);
		
		if(user == null)
			return new User();
		return user;
	}
	
	public int save(User user){
		if(!validateUser(user))
			return -1;
		
		if(user.getUsername().equals(findUserByUsername(user.getUsername())))
			return -2;
		
		user.setRole("USER");
		User savedUser = userDao.save(user);
		return savedUser.getIduser();
	}
	
	private boolean validateUser(User user){
		
		if (user.getUsername() == null || "".equals(user.getUsername())) {
			return false;
		}

		if (user.getPassword() == null || "".equals(user.getPassword())) {
			return false;
		}
		
		return true;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
