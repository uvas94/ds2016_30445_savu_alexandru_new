package org.client.service.model;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the packet database table.
 * 
 */
@Entity
@Table(name="packet")
@NamedQuery(name="Packet.findAll", query="SELECT p FROM Packet p")
public class Packet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idpacket;

	@Column(length=45)
	private String description;

	@Column(nullable=false, length=45)
	private String name;

	@Column(length=450)
	private String route;

	@Column(nullable=false)
	private byte tracking;

	//bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name="currentCity", nullable=false)
	private City city1;

	//bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name="destinationCity", nullable=false)
	private City city2;

	//bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name="senderCity", nullable=false)
	private City city3;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="reciver", nullable=false)
	private User user1;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="sender", nullable=false)
	private User user2;

	public Packet() {
	}

	public int getIdpacket() {
		return this.idpacket;
	}

	public void setIdpacket(int idpacket) {
		this.idpacket = idpacket;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoute() {
		return this.route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public byte getTracking() {
		return this.tracking;
	}

	public void setTracking(byte tracking) {
		this.tracking = tracking;
	}
	 
	public City getCity1() {
		return this.city1;
	}

	public void setCity1(City city1) {
		this.city1 = city1;
	}
	
	public City getCity2() {
		return this.city2;
	}

	public void setCity2(City city2) {
		this.city2 = city2;
	}
	 
	public City getCity3() {
		return this.city3;
	}

	public void setCity3(City city3) {
		this.city3 = city3;
	}
	
	public User getUser1() {
		return this.user1;
	}

	public void setUser1(User user1) {
		this.user1 = user1;
	}
	
	public User getUser2() {
		return this.user2;
	}

	public void setUser2(User user2) {
		this.user2 = user2;
	}

}