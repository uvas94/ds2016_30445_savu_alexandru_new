package org.client.service.settings;

import java.util.Properties;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({"org.client.service"})
@PropertySource(value = {"classpath:application.properties"})
@EnableJpaRepositories(basePackages = "org.client.service.dao")
public class HibernateConfiguration {
	
	@Resource
	private Environment environment;
	
	@Value("${init-db:false}")
	private String initDatabase;
	
	
	@Bean
	   public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
	      LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
	      System.out.println("-----------------------DATA Source----------------");
	      em.setDataSource(dataSource());
	      System.out.println("-----------------------PACKAGE SCAN----------------");
	      em.setPackagesToScan("org.client.service.model");
	      System.out.println("-----------------------Hibenrate----------------");
	      HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
	      vendorAdapter.setGenerateDdl(Boolean.TRUE);
			vendorAdapter.setShowSql(Boolean.TRUE);
	      em.setJpaVendorAdapter(vendorAdapter);
	      System.out.println("-----------------------PRorp----------------");
	      em.setJpaProperties(additionalProperties());
	 
	      return em;
	   }
	 
	   @Bean
	   public DataSource dataSource(){
	      DriverManagerDataSource dataSource = new DriverManagerDataSource();
	     // dataSource.setUrl("jdbc:mysql://localhost:3306/spring_jpa");
	      dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));  
	      dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));     
	      dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
	      dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
	      return dataSource;
	   }
	 
	   @Bean
	   public PlatformTransactionManager transactionManager() {
		   System.out.println("-----------------------Transaction----------------");
	     JpaTransactionManager txManager = new JpaTransactionManager();
	     EntityManagerFactory factory = entityManagerFactory().getObject();
	     txManager.setEntityManagerFactory(factory);
	     return txManager;
	   }
	 
	   @Bean
	   public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
	      return new PersistenceExceptionTranslationPostProcessor();
	   }
	 
	   Properties additionalProperties() {
		   Properties properties = new Properties();
	       properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
	       properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
	       properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
	       return properties;     
	   }

	public Environment getEnvironment() {
		return environment;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	   
	/*
	@Bean
	public PlatformTransactionManager transactionManager(){
		EntityManagerFactory factory = entityManagerFactory().getObject();
		return new JpaTransactionManager(factory);
	}
	
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(){
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(Boolean.TRUE);
		vendorAdapter.setShowSql(Boolean.TRUE);
		
		factory.setDataSource(dataSource());
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("org.client.service.entities");
		
		
		factory.setJpaProperties(hibernateProperties());
		
		factory.afterPropertiesSet();
		factory.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
		return factory;
	}
	
	@Bean
	public HibernateExceptionTranslator hibernateExceptionTranslator(){
		return new HibernateExceptionTranslator();
	}
	

	 @Bean
	    public DataSource dataSource() {
	        DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
	        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
	        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
	        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
	        return dataSource;
	    }
	 
	 @Bean
	 	public DataSourceInitializer dataSourceInitializer(DataSource dataSource){
		 DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
		 dataSourceInitializer.setDataSource(dataSource);
		 ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
		 databasePopulator.addScript(new ClassPathResource("db.sql"));
		 dataSourceInitializer.setDatabasePopulator(databasePopulator);
		 dataSourceInitializer.setEnabled(Boolean.parseBoolean(initDatabase));
		 return dataSourceInitializer;
	 }
	     
	      
	    	@Bean 
	public LocalSessionFactoryBean sessionFactory(){
		LocalSessionFactoryBean sessionFactory  = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory. setPackagesToScan(new String[] {"org.client.service.settings.*"});//org.client.service.settings.model
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	} 
	     
	    private Properties hibernateProperties() {
	        Properties properties = new Properties();
	        properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
	        properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
	        properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
	        return properties;        
	    }
	     
	    @Bean
	    @Autowired
	    public HibernateTransactionManager transactionManager(SessionFactory s) {
	       HibernateTransactionManager txManager = new HibernateTransactionManager();
	       txManager.setSessionFactory(s);
	       return txManager;
	    }*/
}
