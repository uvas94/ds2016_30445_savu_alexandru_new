package org.client.service.dao;

import java.util.List;

import org.client.service.model.Packet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PacketDao extends JpaRepository<Packet, Integer>{
	
	public Packet findByIdpacket(int idpacket);
	public List<Packet> findByName(String name);
	  @Query( value = "SELECT * FROM packet p WHERE p.sender = :id OR p.reciver = :id",
			  nativeQuery=true
		    )
	public List<Packet> findPacketByUser1OrUser2(@Param("id") int userId);
	 
}