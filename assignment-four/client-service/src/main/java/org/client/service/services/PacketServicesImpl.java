package org.client.service.services;

import java.util.ArrayList;
import java.util.Iterator;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;
import org.client.service.dao.CityDao;
import org.client.service.dao.PacketDao;
import org.client.service.dao.UserDao;
import org.client.service.model.City;
import org.client.service.model.ListWrapper;
import org.client.service.model.Packet;
import org.client.service.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@WebService(
		serviceName = "PacketService",
		targetNamespace  = "services.server.web.four.assig.org",
		endpointInterface = "org.client.service.services.PacketServices")
@XmlRootElement
public class PacketServicesImpl implements PacketServices {

	
	@Autowired
	private PacketDao packetDao;
	
	@Autowired
	private CityDao cityDao;
	
	public PacketServicesImpl() {
		// TODO Auto-generated constructor stub
	}
	
	public ListWrapper<Packet> getAllUserPackages(User user) {
		ArrayList<Packet> allUserPackets = (ArrayList<Packet>) packetDao.findPacketByUser1OrUser2(user.getIduser());
		
		if(allUserPackets != null)
			return new ListWrapper<Packet>(allUserPackets);
		
		return  new ListWrapper<Packet>(new ArrayList<Packet>());
	}

	public ListWrapper<City> getAllCities() {
		ArrayList<City> allCities = (ArrayList<City>) cityDao.findAll();
		if (allCities != null)
		return new ListWrapper<City>(allCities);
		return new ListWrapper<City>(new ArrayList<City>());
	}

	public City getCurrentCity(Packet packet) {
		Packet updatePacket = packetDao.findByIdpacket(packet.getIdpacket());
		if(updatePacket == null)
			return new City();
		City city = updatePacket.getCity3();
		if(city != null)
			return city;
		return new City();
	}

	public ListWrapper<Packet> searchPacketByName(String name,User user) {
		//ArrayList<Packet> packet = (ArrayList<Packet>) packetDao.findByName(name);
		ListWrapper<Packet> packets =  getAllUserPackages(user);
		ArrayList<Packet> packet = new ArrayList<Packet>();
		Iterator<Packet> it = packets.getIterator();
		while(it.hasNext()){
			Packet p = it.next();
			if(p.getName().equals(name))
				packet.add(p);
		}
			return new ListWrapper<Packet>(packet);
	}


	public int saveUserPacket(Packet packet) {
		// TODO Auto-generated method stub
		if(verify(packet))
			return -1;
		packet.setTracking((byte) 0);
		Packet returnedPacket = packetDao.save(packet);
		return returnedPacket.getIdpacket();
	}
	

	public int deleteUserPacket(Packet packet) {

		if(packet == null)
			return -1;
		packetDao.delete(packet);
		return packet.getIdpacket();
	}
	
	@Autowired
	private UserDao userDao;
	
	
	public User findUserByUsername(String username){
		 
		if(userDao == null)
			return new User();
		User user = userDao.findByUsername(username);
		
		if(user == null)
			return new User();
		return user;
	}
	
	public int save(User user){
		if(!validateUser(user))
			return -1;
		User u = findUserByUsername(user.getUsername());
		if(u != null)
			return -2;
		
		user.setRole("USER");
		User savedUser = userDao.save(user);
		return savedUser.getIduser();
	}
	
//	
//	public ArrayList<User> getAllUsers() {
//		ArrayList<User> users = (ArrayList<User>) userDao.findAll();
//		if (users == null)
//			return new ArrayList<User>();
//		return users;
//	}
	
	private boolean validateUser(User user){
		
		if (user.getUsername() == null || "".equals(user.getUsername())) {
			return false;
		}

		if (user.getPassword() == null || "".equals(user.getPassword())) {
			return false;
		}
		
		return true;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	public PacketDao getPacketDao() {
		return packetDao;
	}

	public void setPacketDao(PacketDao packetDao) {
		this.packetDao = packetDao;
	}

	public CityDao getCityDao() {
		return cityDao;
	}

	public void setCityDao(CityDao cityDao) {
		this.cityDao = cityDao;
	}
	
	
	
	private boolean verify(Packet packet){
		
		if(packet.getName() == null)
			return false;
		if(packet.getCity1() == null)
			return false;
		if(packet.getCity2() == null)
			return false;
		if(packet.getCity3() == null)
			return false;
		if(packet.getUser1() == null)
			return false;
		if(packet.getUser2() == null)
			return false;
		
		return true;
	}

}
