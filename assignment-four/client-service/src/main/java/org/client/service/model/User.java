package org.client.service.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;

import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Table(name="user")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int iduser;

	@Column(nullable=false, length=45)
	private String password;

	@Column(nullable=false, length=45)
	private String role;

	@Column(nullable=false, length=30)
	private String username;

	//bi-directional many-to-one association to Packet
	@OneToMany(fetch = FetchType.EAGER,mappedBy="user1")
	private List<Packet> packets1;

	//bi-directional many-to-one association to Packet
	@OneToMany(fetch = FetchType.EAGER,mappedBy="user2")
	private List<Packet> packets2;

	public User() {
	}

	public int getIduser() {
		return this.iduser;
	}

	public void setIduser(int iduser) {
		this.iduser = iduser;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	@XmlTransient
	public List<Packet> getPackets1() {
		return this.packets1;
	}

	public void setPackets1(List<Packet> packets1) {
		this.packets1 = packets1;
	}

	public Packet addPackets1(Packet packets1) {
		getPackets1().add(packets1);
		packets1.setUser1(this);

		return packets1;
	}

	public Packet removePackets1(Packet packets1) {
		getPackets1().remove(packets1);
		packets1.setUser1(null);

		return packets1;
	}
	@XmlTransient
	public List<Packet> getPackets2() {
		return this.packets2;
	}

	public void setPackets2(List<Packet> packets2) {
		this.packets2 = packets2;
	}

	public Packet addPackets2(Packet packets2) {
		getPackets2().add(packets2);
		packets2.setUser2(this);

		return packets2;
	}
	
	public Packet removePackets2(Packet packets2) {
		getPackets2().remove(packets2);
		packets2.setUser2(null);

		return packets2;
	}

}