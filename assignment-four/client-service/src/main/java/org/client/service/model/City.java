package org.client.service.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;

import java.util.List;


/**
 * The persistent class for the city database table.
 * 
 */
@Entity
@Table(name="city")
@NamedQuery(name="City.findAll", query="SELECT c FROM City c")
public class City implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idcity;

	@Column(nullable=false, length=45)
	private String cityName;

	@Column(nullable=false)
	private double lat;

	@Column(name="long", nullable=false)
	private double long_;

	//bi-directional many-to-one association to Packet
	@OneToMany(fetch = FetchType.EAGER,mappedBy="city1")
	private List<Packet> packets1;

	//bi-directional many-to-one association to Packet
	@OneToMany(fetch = FetchType.EAGER,mappedBy="city2")
	private List<Packet> packets2;

	//bi-directional many-to-one association to Packet
	@OneToMany(fetch = FetchType.EAGER,mappedBy="city3")
	private List<Packet> packets3;

	public City() {
	}

	public int getIdcity() {
		return this.idcity;
	}

	public void setIdcity(int idcity) {
		this.idcity = idcity;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public double getLat() {
		return this.lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLong_() {
		return this.long_;
	}

	public void setLong_(double long_) {
		this.long_ = long_;
	}
	 @XmlTransient
	public List<Packet> getPackets1() {
		return this.packets1;
	}
	
	public void setPackets1(List<Packet> packets1) {
		this.packets1 = packets1;
	}

	public Packet addPackets1(Packet packets1) {
		getPackets1().add(packets1);
		packets1.setCity1(this);

		return packets1;
	}

	public Packet removePackets1(Packet packets1) {
		getPackets1().remove(packets1);
		packets1.setCity1(null);

		return packets1;
	}
	 @XmlTransient
	public List<Packet> getPackets2() {
		return this.packets2;
	}

	public void setPackets2(List<Packet> packets2) {
		this.packets2 = packets2;
	}

	public Packet addPackets2(Packet packets2) {
		getPackets2().add(packets2);
		packets2.setCity2(this);

		return packets2;
	}

	public Packet removePackets2(Packet packets2) {
		getPackets2().remove(packets2);
		packets2.setCity2(null);

		return packets2;
	}
	
	 @XmlTransient
	public List<Packet> getPackets3() {
		return this.packets3;
	}

	public void setPackets3(List<Packet> packets3) {
		this.packets3 = packets3;
	}

	public Packet addPackets3(Packet packets3) {
		getPackets3().add(packets3);
		packets3.setCity3(this);

		return packets3;
	}

	public Packet removePackets3(Packet packets3) {
		getPackets3().remove(packets3);
		packets3.setCity3(null);

		return packets3;
	}

}