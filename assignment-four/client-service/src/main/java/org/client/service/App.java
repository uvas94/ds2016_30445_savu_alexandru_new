package org.client.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.client.service.model.City;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Hello world!
 *
 */
@Configuration
@ComponentScan(basePackages = "org.client.service")
public class App 
{
   public static void  main(String [] argc){
	   List<Pair<City,Date>> list = new ArrayList<Pair<City,Date>>();
       for(int i = 0; i <= 3; i++){
       City _city = new City();
       _city.setCityName("Cluj");
       Pair<City,Date> pair = new Pair<City,Date>();
       pair.First = _city;
       pair.Second = new Date();
       list.add(pair);
       }
       
       GsonBuilder builder = new GsonBuilder();
       Gson gson = builder.create();
       System.out.println(gson.toJson(list));
   }
   
}
class Pair<T,U>
{
    public Pair() { }
    public Pair(T first, U second)
    {
        this.First = first;
        this.Second = second;
    }
    public T First ;
    public T getFirst(){return First;}
    public void setFirst(T First) {this.First = First;}
    public U Second ;
    public U getSecond(){return Second;} 
    public void setSecond(U Second){this.Second = Second;}
}
