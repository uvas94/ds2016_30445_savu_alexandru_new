package org.client.service.dao;

import org.client.service.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User,Integer>{
	
	User findByUsername(String username);
	
}
