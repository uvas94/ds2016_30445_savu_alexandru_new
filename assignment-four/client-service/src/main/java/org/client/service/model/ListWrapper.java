package org.client.service.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ListWrapper<T> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<T> list;
	
	public ListWrapper()
	{
		this.list = new ArrayList<T>();
	}
	public ListWrapper(ArrayList<T> list)
	{
		this.list = list;
	}
	
	@XmlElement
	public List<T> getList(){
		return list;
	}
	
	public void removeItem(T item){
		list.remove(item);
	}
	
	public void addItem(T item){
		list.add(item);
	}
	
	public int size(){return list.size();}
	
	public Iterator<T> getIterator(){return list.iterator();}
	
}
