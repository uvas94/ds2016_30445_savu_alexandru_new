<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Resources/LoginStylesheet.css">
</head>
<body>
		<form:form commandName="user" name="loginForm" action = "${pageContext.request.contextPath}/registration" method = "post"> 
			<p>Registration</p>
		
		<form:input path = "username" type="text" id="username" name="username" placeholder="Username"/>
		<form:input path = "password" type="password" id="password" name="password" placeholder="Password"/>
		<c:if test="${!empty error}">
		<p> ${error} </p>
		</c:if>
		<input type = "submit" name = "Submit" value = "Submit"><br>	
	</form:form>
</body>
</html>