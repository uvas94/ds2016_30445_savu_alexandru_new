<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Packets</title>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Resources/AdminStylesheet.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
		<script type="text/javascript" src="/travel-request/resources/js/json.min.js"></script>
		<script language="javascript" type="text/javascript">
		var updateUrl = "${pageContext.request.contextPath}/user/getLocalTime";
		var logoutUrl = "${pageContext.request.contextPath}/logout"
		
			
		function setLogoutForm() {
			//document.cityForm.methodd.value = "logout";
			document.searchForm.action =   logoutUrl;
			document.searchForm.method = "get";
		}


		setInterval(update,5000);
		
		function update() {
	      
	        $.ajax({
	        url: "${pageContext.request.contextPath}/user/update",
	        success: function(response){
		        
	        	  var td = $("tr>td:nth-last-child(2)");
                 
                  $.each(response, function(index, item) {
                      td[index].innerText = item;
                  });
	        },
	        error: function(e){
	        //alert('Error: ' + e);
	        	console.log('Error:', data);
	        }
	        });
	        }
        
		</script>
		
	</head>
	<body>
		<fieldset>
		<center>
		<form  action="${pageContext.request.contextPath}/user/search" name="searchForm" method="post">
		<table>
			<tr><td>Search:<input name ="Search" id="Search"/></td></tr>
			
			<tr><td colspan="1">
			<input type="submit" name="searchBtn" value="Find"/>
			&nbsp;<input type="submit" name="logout" value="logout" onclick="setLogoutForm();"/></td></tr>
			
		</table>
		</form>
		</center>
		</fieldset>
		<c:if test="${!empty packages}">
		<fieldset >
			<br />
			<center>
				<table width="90%">
					<tr style="background-color: #093145;">
						<th>Name</th>
						<th>Description</th>
						<th>Sender</th>
						<th>Reciver</th>
						<th>Sender city</th>
						<th>Destination city</th>
						<th>Current city</th>
						<th>Tracking</th>
					</tr>
					<c:forEach items="${packages}" var="package1">
					
						<tr style="background-color: #107896;">
						
							<td><c:out value="${package1.name}"/></td>
							<td><c:out value="${package1.description}"/></td>
							<td><c:out value="${package1.user2.username}"/></td>
							<td><c:out value="${package1.user1.username}"/></td>
							<td><c:out value="${package1.city3.cityName}"/></td>
							<td><c:out value="${package1.city2.cityName}"/></td>
							<td><c:out	value="${package1.city1.cityName}" /></td>
							<td><c:out value="${package1.tracking}"/></td>

						</tr>

					</c:forEach>
				</table>
				</center>
			<br />
		</fieldset>
		</c:if>
	</body>
</html>