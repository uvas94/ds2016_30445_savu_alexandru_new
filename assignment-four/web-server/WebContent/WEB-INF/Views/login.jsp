<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Resources/LoginStylesheet.css">
</head>
<body>
	

	<form name="loginForm" action = "${pageContext.request.contextPath}/login" method = "post"> 
	<p>Log in</p>
	
		 <input type="text" id="username" name="username" placeholder="Username">
		
		<input type="password" id="password" name="password" placeholder="Password">
		<p> ${error} </p>
		<input type = "submit" name = "login" value = "login"><br>	
		<a href= "${pageContext.request.contextPath}/registration">Create an account</a>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
		
	
</body>
</html>