<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1251"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Resources/AdminStylesheet.css"/>
		
		<script language="javascript" type="text/javascript">
		var newPacketId;
		var addUrl = "${pageContext.request.contextPath}/admin/addPacket";
		var deleteUrl = "${pageContext.request.contextPath}/admin/deletePacket";
		var updateUrl = "${pageContext.request.contextPath}/admin/updatePacket";
		var logoutUrl = "${pageContext.request.contextPath}/login"
		
		function setUpdateForm(element){

			newPacketId = document.packetForm.idpacket.value;
			trElement = document.getElementById(element);
			//changeSelectValue(document.packetForm.role, trElement.children[2].innerHTML);
			
			document.packetForm.idpacket.value = trElement.children[1].innerHTML;
			document.packetForm.name.value = trElement.children[2].innerHTML;
			document.packetForm.description.value = trElement.children[3].innerHTML;
			
			selectedOption(trElement.children[4].innerHTML,document.packetForm.sender)
			
			selectedOption(trElement.children[5].innerHTML,document.packetForm.reciver)
			
			selectedOption(trElement.children[6].innerHTML,document.packetForm.sendercity)
			
			document.packetForm.currentcity.value = trElement.children[7].innerHTML;
			
			selectedOption(trElement.children[8].innerHTML,document.packetForm.destinationcity);
			
			document.packetForm.tracking.value = trElement.children[9].innerHTML;
			document.packetForm.route.value = trElement.children[10].innerHTML;
			 
	
			document.packetForm.newPacket.disabled = false;
			document.packetForm.deletePacket.disabled = false;
			document.packetForm.action = updateUrl;
		}

		function selectedOption(city,element){
			
			var i = 0; 
			var options =  document.getElementById(element.id).options;
			for( i = 0 ; i <= options.length; i++)
				if(options[i].value == city)
				{	options[i].selected = true;
				return true;
				}
			return false;
		}
	
		function setAddForm(){
			document.packetForm.idpacket.value = newPacketId;
			document.packetForm.name.value = "";
			document.packetForm.description.value = "";
			document.getElementById("sender").selectedIndex = 0;
			document.getElementById("reciver").selectedIndex = 0;
			document.getElementById("sendercity").selectedIndex = 0;
			document.getElementById("departureCity").selectedIndex = 0;

			document.packetForm.currentcity.value = 0; 

			document.packetForm.tracking.value = false;
			document.packetForm.route.value = "";
			
			
			document.packetForm.newPacket.disabled = true;
			document.packetForm.deletePacket.disabled = true;
			document.packetForm.action =  addUrl;
		}

		function setDeleteForm() {
			document.packetForm.action =   deleteUrl;
		}
		function setLogoutForm() {
			document.packetForm.action =   logoutUrl;
			document.packetForm.method = "get";
		}
		</script>
		
	</head>
	<body>
		<fieldset>
		
		<center>
		<form:form commandName="package" action="${pageContext.request.contextPath}/admin/addPacket" name="packetForm" method="post">
		<form:hidden path="idpacket"/>
			<form:hidden path="currentcity" />
			<form:hidden path="route"/>
			<form:hidden path="tracking"/>
			<form:hidden path="route"/>
		<table>
			<tr><td><c:out value ="${requestScope.msg}"/> </td></tr>
			<tr>	<td>Name</td>		<td> <form:input path="name" type = "text"/></td>			</tr>
			<tr>	<td>Description </td>	<td> <form:input path="description" type = "text"/></td>	</tr>
			<tr>		<td>Sender</td>
										<td> 											
												 <form:select path="sender">
												 	<c:forEach var="theUser" items="${users}">
        												<form:option value="${theUser.iduser}"><c:out value="${theUser.username}"/></form:option>
   													 </c:forEach>	
												 	
												 </form:select>											
										</td>
			</tr>
			<tr>		<td>Reciver</td>
										<td> 
												 <form:select path="reciver">
												 	<c:forEach var="theUser" items="${users}">
        												<form:option value="${theUser.iduser}"><c:out value="${theUser.username}"/></form:option>
   													 </c:forEach>	
												 </form:select>											 
										</td>
			</tr>
			<tr>		<td>Sender city</td>
										<td> 											
												 <form:select path="sendercity">
												 	<c:forEach var="city" items="${cities}">
        												<form:option value="${city.idcity}"><c:out value="${city.cityname}"/></form:option>
   													 </c:forEach>	
												 </form:select>											
										</td>
			</tr>			
			<tr>		<td>Destination city</td>
										<td> 
												 <form:select path="destinationcity">
												 	<c:forEach var="city" items="${cities}">
        												<form:option value="${city.idcity}"><c:out value="${city.cityname}"/></form:option>
   													 </c:forEach>	
												 </form:select>											 
										</td>
			</tr>
			
			<tr><td colspan="2">
			<input type="submit" value="Save Packet"/>
			&nbsp;<input type="reset" name="newPacket" value="New Packet" onclick="setAddForm();" disabled="disabled"/>
			&nbsp;<input type="submit" name="deletePacket" value="Delete Packet" onclick="setDeleteForm();" disabled="disabled"/>
			&nbsp;<input type="submit" name="logout" value="logout" onclick="setLogoutForm();"/></td></tr>
			
		</table>
		</form:form>
		</center>
		</fieldset>
		<c:if test="${!empty packages}">
		<fieldset >
			<br />
			<center>
				<table width="90%">
					<tr style="background-color: #093145;">
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Sender</th>
						<th>Reciver</th>
						<th>Sender city</th>
						<th>Current city</th>
						<th>Destination city</th>
						<th>Tracked</th>
						<th>Track</th>
					</tr>
					<c:forEach items="${packages}" var="package1">
					
						<tr style="background-color: #107896;" id="f${package1.idpacket}" onclick="setUpdateForm('f${package1.idpacket}');">
						<form:form  commandName="packageT" action="${pageContext.request.contextPath}/admin/track" name="trackForm" method="post">
							<form:hidden path="idpacket" value="${package1.idpacket}"/>
							<td><c:out value="${package1.idpacket}"/></td>
							<td><c:out value="${package1.name}"/></td>
							<td><c:out value="${package1.description}"/></td>
							<td><c:out value="${package1.sender}"/></td>
							<td><c:out value="${package1.reciver}"/></td>
							<td><c:out value="${package1.sendercity}"/></td>
							<td><c:out value="${package1.currentcity}"/></td>
							<td><c:out value="${package1.destinationcity}"/></td>
							<td><c:out value="${package1.tracking}"/></td>
							<td style="display:none;"><c:out value ="${package1.route }"/></td>
							<td><input style="width:100%" type="submit" name = "track" value="Track" /></td>
							</form:form>
						</tr>
						
					
					</c:forEach>
				</table>
				</center>
			<br />
		</fieldset>
		</c:if>
	</body>
</html>