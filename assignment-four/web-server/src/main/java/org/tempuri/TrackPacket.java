
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.admin_service.ArrayOfPair;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idpacket" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="route" type="{http://schemas.datacontract.org/2004/07/admin_service}ArrayOfPair" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idpacket",
    "route"
})
@XmlRootElement(name = "trackPacket")
public class TrackPacket {

    protected Long idpacket;
    @XmlElementRef(name = "route", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPair> route;

    /**
     * Gets the value of the idpacket property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdpacket() {
        return idpacket;
    }

    /**
     * Sets the value of the idpacket property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdpacket(Long value) {
        this.idpacket = value;
    }

    /**
     * Gets the value of the route property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPair }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPair> getRoute() {
        return route;
    }

    /**
     * Sets the value of the route property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPair }{@code >}
     *     
     */
    public void setRoute(JAXBElement<ArrayOfPair> value) {
        this.route = value;
    }

}
