
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.datacontract.schemas._2004._07.admin_service.ArrayOfPair;
import org.datacontract.schemas._2004._07.assigfourcontext.ArrayOfCity;
import org.datacontract.schemas._2004._07.assigfourcontext.ArrayOfPacket;
import org.datacontract.schemas._2004._07.assigfourcontext.ArrayOfUser;
import org.datacontract.schemas._2004._07.assigfourcontext.City;
import org.datacontract.schemas._2004._07.assigfourcontext.Packet;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TrackPacketRoute_QNAME = new QName("http://tempuri.org/", "route");
    private final static QName _GetAllCitiesResponseGetAllCitiesResult_QNAME = new QName("http://tempuri.org/", "getAllCitiesResult");
    private final static QName _UpdatePacketPacket_QNAME = new QName("http://tempuri.org/", "packet");
    private final static QName _GetCityByNameResponseGetCityByNameResult_QNAME = new QName("http://tempuri.org/", "getCityByNameResult");
    private final static QName _GetAllPacketsResponseGetAllPacketsResult_QNAME = new QName("http://tempuri.org/", "getAllPacketsResult");
    private final static QName _GetCityByNameName_QNAME = new QName("http://tempuri.org/", "name");
    private final static QName _GetAllUsersResponseGetAllUsersResult_QNAME = new QName("http://tempuri.org/", "getAllUsersResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeletePacketResponse }
     * 
     */
    public DeletePacketResponse createDeletePacketResponse() {
        return new DeletePacketResponse();
    }

    /**
     * Create an instance of {@link GetAllUsers }
     * 
     */
    public GetAllUsers createGetAllUsers() {
        return new GetAllUsers();
    }

    /**
     * Create an instance of {@link UpdatePacketResponse }
     * 
     */
    public UpdatePacketResponse createUpdatePacketResponse() {
        return new UpdatePacketResponse();
    }

    /**
     * Create an instance of {@link GetCityByName }
     * 
     */
    public GetCityByName createGetCityByName() {
        return new GetCityByName();
    }

    /**
     * Create an instance of {@link TrackPacketResponse }
     * 
     */
    public TrackPacketResponse createTrackPacketResponse() {
        return new TrackPacketResponse();
    }

    /**
     * Create an instance of {@link RetrunI }
     * 
     */
    public RetrunI createRetrunI() {
        return new RetrunI();
    }

    /**
     * Create an instance of {@link TrackPacket }
     * 
     */
    public TrackPacket createTrackPacket() {
        return new TrackPacket();
    }

    /**
     * Create an instance of {@link GetAllPacketsResponse }
     * 
     */
    public GetAllPacketsResponse createGetAllPacketsResponse() {
        return new GetAllPacketsResponse();
    }

    /**
     * Create an instance of {@link GetAllPackets }
     * 
     */
    public GetAllPackets createGetAllPackets() {
        return new GetAllPackets();
    }

    /**
     * Create an instance of {@link SavePacket }
     * 
     */
    public SavePacket createSavePacket() {
        return new SavePacket();
    }

    /**
     * Create an instance of {@link DeletePacket }
     * 
     */
    public DeletePacket createDeletePacket() {
        return new DeletePacket();
    }

    /**
     * Create an instance of {@link UpdatePacket }
     * 
     */
    public UpdatePacket createUpdatePacket() {
        return new UpdatePacket();
    }

    /**
     * Create an instance of {@link GetAllUsersResponse }
     * 
     */
    public GetAllUsersResponse createGetAllUsersResponse() {
        return new GetAllUsersResponse();
    }

    /**
     * Create an instance of {@link SavePacketResponse }
     * 
     */
    public SavePacketResponse createSavePacketResponse() {
        return new SavePacketResponse();
    }

    /**
     * Create an instance of {@link RetrunIResponse }
     * 
     */
    public RetrunIResponse createRetrunIResponse() {
        return new RetrunIResponse();
    }

    /**
     * Create an instance of {@link GetAllCities }
     * 
     */
    public GetAllCities createGetAllCities() {
        return new GetAllCities();
    }

    /**
     * Create an instance of {@link GetAllCitiesResponse }
     * 
     */
    public GetAllCitiesResponse createGetAllCitiesResponse() {
        return new GetAllCitiesResponse();
    }

    /**
     * Create an instance of {@link GetCityByNameResponse }
     * 
     */
    public GetCityByNameResponse createGetCityByNameResponse() {
        return new GetCityByNameResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPair }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "route", scope = TrackPacket.class)
    public JAXBElement<ArrayOfPair> createTrackPacketRoute(ArrayOfPair value) {
        return new JAXBElement<ArrayOfPair>(_TrackPacketRoute_QNAME, ArrayOfPair.class, TrackPacket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getAllCitiesResult", scope = GetAllCitiesResponse.class)
    public JAXBElement<ArrayOfCity> createGetAllCitiesResponseGetAllCitiesResult(ArrayOfCity value) {
        return new JAXBElement<ArrayOfCity>(_GetAllCitiesResponseGetAllCitiesResult_QNAME, ArrayOfCity.class, GetAllCitiesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Packet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "packet", scope = UpdatePacket.class)
    public JAXBElement<Packet> createUpdatePacketPacket(Packet value) {
        return new JAXBElement<Packet>(_UpdatePacketPacket_QNAME, Packet.class, UpdatePacket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Packet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "packet", scope = SavePacket.class)
    public JAXBElement<Packet> createSavePacketPacket(Packet value) {
        return new JAXBElement<Packet>(_UpdatePacketPacket_QNAME, Packet.class, SavePacket.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link City }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getCityByNameResult", scope = GetCityByNameResponse.class)
    public JAXBElement<City> createGetCityByNameResponseGetCityByNameResult(City value) {
        return new JAXBElement<City>(_GetCityByNameResponseGetCityByNameResult_QNAME, City.class, GetCityByNameResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPacket }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getAllPacketsResult", scope = GetAllPacketsResponse.class)
    public JAXBElement<ArrayOfPacket> createGetAllPacketsResponseGetAllPacketsResult(ArrayOfPacket value) {
        return new JAXBElement<ArrayOfPacket>(_GetAllPacketsResponseGetAllPacketsResult_QNAME, ArrayOfPacket.class, GetAllPacketsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "name", scope = GetCityByName.class)
    public JAXBElement<String> createGetCityByNameName(String value) {
        return new JAXBElement<String>(_GetCityByNameName_QNAME, String.class, GetCityByName.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getAllUsersResult", scope = GetAllUsersResponse.class)
    public JAXBElement<ArrayOfUser> createGetAllUsersResponseGetAllUsersResult(ArrayOfUser value) {
        return new JAXBElement<ArrayOfUser>(_GetAllUsersResponseGetAllUsersResult_QNAME, ArrayOfUser.class, GetAllUsersResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Packet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "packet", scope = DeletePacket.class)
    public JAXBElement<Packet> createDeletePacketPacket(Packet value) {
        return new JAXBElement<Packet>(_UpdatePacketPacket_QNAME, Packet.class, DeletePacket.class, value);
    }

}
