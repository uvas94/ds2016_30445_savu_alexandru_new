
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.assigfourcontext.Packet;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="packet" type="{http://schemas.datacontract.org/2004/07/AssigfourContext}Packet" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "packet"
})
@XmlRootElement(name = "savePacket")
public class SavePacket {

    @XmlElementRef(name = "packet", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<Packet> packet;

    /**
     * Gets the value of the packet property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Packet }{@code >}
     *     
     */
    public JAXBElement<Packet> getPacket() {
        return packet;
    }

    /**
     * Sets the value of the packet property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Packet }{@code >}
     *     
     */
    public void setPacket(JAXBElement<Packet> value) {
        this.packet = value;
    }

}
