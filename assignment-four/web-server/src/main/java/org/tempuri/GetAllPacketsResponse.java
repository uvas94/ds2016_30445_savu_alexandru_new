
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.assigfourcontext.ArrayOfPacket;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getAllPacketsResult" type="{http://schemas.datacontract.org/2004/07/AssigfourContext}ArrayOfPacket" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllPacketsResult"
})
@XmlRootElement(name = "getAllPacketsResponse")
public class GetAllPacketsResponse {

    @XmlElementRef(name = "getAllPacketsResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPacket> getAllPacketsResult;

    /**
     * Gets the value of the getAllPacketsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPacket }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPacket> getGetAllPacketsResult() {
        return getAllPacketsResult;
    }

    /**
     * Sets the value of the getAllPacketsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPacket }{@code >}
     *     
     */
    public void setGetAllPacketsResult(JAXBElement<ArrayOfPacket> value) {
        this.getAllPacketsResult = value;
    }

}
