package org.assig.four.web.server.settings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "org.assig.four.web.server")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	CostumSuccessHandler costumSuccessHandler;
	@Autowired
	LoginDAO loginDao;
	
	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(loginDao);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		
		http.authorizeRequests()
		.antMatchers("/", "/login","/registration").permitAll()	
		.antMatchers("/admin/**").access("hasRole('ADMIN')")
		.antMatchers("/user/**").access("hasRole('ADMIN') or hasRole('USER')")
		.antMatchers("/Resources/**").permitAll().anyRequest().authenticated()
		.and().formLogin().loginPage("/login").successHandler(costumSuccessHandler).permitAll()
		.and().exceptionHandling().accessDeniedPage("/Access_Denied")
		.and().logout().permitAll();

	}
}
