package org.assig.four.web.server.dto;

public class Pair<T,U> {

	  public Pair() { }
      public Pair(T first, U second)
      {
          this.first = first;
          this.second = second;
      }
       
      private T first;
      private U second;
      public T getFirst(){return this.first;} 
      public void setFirst(T first){ this.first=first; }
       
      public U getSecond(){return this.second;} 
      public void setSecond(U second){ this.second=second; }

}
