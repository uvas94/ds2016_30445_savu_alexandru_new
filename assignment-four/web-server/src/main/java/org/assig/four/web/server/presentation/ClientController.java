package org.assig.four.web.server.presentation;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.assig.four.web.server.dto.Pair;
import org.assig.four.web.server.services.PacketService;
import org.assig.four.web.server.services.PacketServices;
import org.assig.four.web.server.services.packetservices.ListWrapper;
import org.assig.four.web.server.services.packetservices.Packet;
import org.assig.four.web.server.services.packetservices.User;
import org.datacontract.schemas._2004._07.admin_service.ArrayOfPair;
import org.datacontract.schemas._2004._07.assigfourcontext.ArrayOfCity;
import org.datacontract.schemas._2004._07.assigfourcontext.ArrayOfPacket;
import org.datacontract.schemas._2004._07.assigfourcontext.ArrayOfUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.tempuri.WebService;
import org.tempuri.WebServer;


@Controller
@RequestMapping("/*")
public class ClientController {
	
	
	PacketServices packetService;
	WebService adminService;
	public ClientController() {
		//userService = new UserService().getUserServicesImplPort();
		try{
		packetService = new PacketService().getPacketServicesImplPort();
		adminService = new WebServer().getBasicHttpBindingWebService();
		}
		catch(Exception e){
			System.out.println("Error in controller:"+e);
		}
	}
	
	@RequestMapping(value="login", method=RequestMethod.GET)
	public String loginPage(){
		return "login";
	}
	
	@RequestMapping(value="registration",method = RequestMethod.GET)
	public String getRegistrationPage(ModelMap model){
		User user = new User();
		model.addAttribute("user",user);
		return "registration";
	}
	@RequestMapping(value="registration",method =  RequestMethod.POST)
	public String postRegistrationPage(@ModelAttribute(value="user") User user,BindingResult result,HttpServletRequest request){
		
		
		int user1 = packetService.save(user);
		
		switch(user1){
		case -1:
			request.setAttribute("error", "Wrong username or password");
			break;
		case -2:
			request.setAttribute("error", "Username already exists");
			break;
		default:	
		
		}
		
	
		return "login";
	}
	
	@RequestMapping(value="user/search", method = RequestMethod.POST)
	public String postSearchPage(ModelMap model,HttpServletRequest request){
		
		String search = request.getParameter("Search");
		User logedUser = packetService.findUserByUsername(getPrincipal());
		ListWrapper packages = packetService.searchPacketByName(search, logedUser);
		model.addAttribute("packages",packages.getList());
		
		return "user";
	}
	
	@RequestMapping(value = "user/update", method = RequestMethod.GET)
	public @ResponseBody List<String> getUpdateAjax() {
		List<String> response = new ArrayList<String>();
		
		User logedUser = packetService.findUserByUsername(getPrincipal());
		ListWrapper packets = packetService.getAllUserPackages(logedUser);
		
		//List<String> currentCity = new ArrayList<String>();
		List<Object> lp = packets.getList();
		for(Object p:lp){
			response.add( ((Packet) p).getCity1().getCityName() );
		}
		
		return response;
	}
	
	@RequestMapping(value="user",method =  RequestMethod.GET)
	public String getUserPage(ModelMap model){
		
		User logedUser = packetService.findUserByUsername(getPrincipal());
		ListWrapper packages = packetService.getAllUserPackages(logedUser);
		
		//ArrayList<Package> packages = new ArrayList<Package>();
//		Packet packet = new Packet();
//		ArrayList cities = packetService.getAllCities();
//		ArrayList users = packetService.getAllUsers();
//		
//		model.addAttribute("",cities);
//		model.addAttribute("",cities);
//		model.addAttribute("",users);
//		model.addAttribute("",users);
 		model.addAttribute("packages", packages.getList());
		
		return "user";
	}
	
	@RequestMapping(value = "Access_Denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("user", getPrincipal());
		return "accessDenied";
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@RequestMapping(value = "admin", method = RequestMethod.GET)
	public String getAdminPage(ModelMap model){
		
//		org.datacontract.schemas._2004._07.assigfourcontext.Packet packett =new org.datacontract.schemas._2004._07.assigfourcontext.Packet();
//		packett.setIdpacket((long) 1);
//		
//		adminService.deletePacket(packett);
		
		

		ArrayOfPacket packages = adminService.getAllPackets();
		ArrayOfCity cities = adminService.getAllCities();
		ArrayOfUser users = adminService.getAllUsers();
		//PacketDTO pckt =new PacketDTO();
		 org.datacontract.schemas._2004._07.assigfourcontext.Packet pckt = new org.datacontract.schemas._2004._07.assigfourcontext.Packet();
		 org.datacontract.schemas._2004._07.assigfourcontext.Packet pcktt = new org.datacontract.schemas._2004._07.assigfourcontext.Packet();
		model.addAttribute("package",pckt);
		model.addAttribute("packageT", pcktt);
		model.addAttribute("users",users.getUser());
		model.addAttribute("cities", cities.getCity());
		model.addAttribute("packages",packages.getPacket());
		return "admin";
	}
	
	@RequestMapping(value = "admin/addPacket", method = RequestMethod.POST)
	public String postAddPage(@ModelAttribute(value="package") org.datacontract.schemas._2004._07.assigfourcontext.Packet packet,ModelMap model){
		
		packet.setCurrentcity(packet.getSendercity());
		packet.setTracking(false);
		
		packet.setRoute("");
		//org.datacontract.schemas._2004._07.assigfourcontext.Packet pckt = new org.datacontract.schemas._2004._07.assigfourcontext.Packet();
		//pckt.setCurrentcity(packet.getCurrentcity());
		
		//pckt.setDescription(new JAXBElement(packet.getDescription()));
		adminService.savePacket(packet);
		
		return getAdminPage(model);
	}
	
	@RequestMapping(value = "admin/deletePacket", method = RequestMethod.POST)
	public String postDeletePage(@ModelAttribute(value="packet") org.datacontract.schemas._2004._07.assigfourcontext.Packet packet,ModelMap model){
		
		
		adminService.deletePacket(packet);
		
		return getAdminPage(model);
	}
	
	@RequestMapping(value = "admin/updatePacket", method = RequestMethod.POST)
	public String postUpdatePage(@ModelAttribute(value="packet") org.datacontract.schemas._2004._07.assigfourcontext.Packet packet,ModelMap model){
		
		//packet.setCurrentcity(packet.getSender());
		//packet.setTracking(false);
		
		//packet.setRoute(null);
		adminService.updatePacket(packet);
		
		return getAdminPage(model);
	}
	@RequestMapping(value = "admin/track", method = RequestMethod.POST)
	public String postTrackPage(@ModelAttribute(value="package1") org.datacontract.schemas._2004._07.assigfourcontext.Packet packet,ModelMap model){
		
		//packet.setCurrentcity(packet.getSender());
		//packet.setTracking(false);
		
		//packet.setRoute(null);
		Pair<Long, List<Pair<Long,String>>> pair = new Pair<Long, List<Pair<Long,String>>>(packet.getIdpacket(), new ArrayList<Pair<Long,String>>());
		
		//pair.setFirst(packet.getIdpacket());
		
		ArrayOfCity cities = adminService.getAllCities();
		model.addAttribute("cities",cities.getCity());
		model.addAttribute("packet", packet);
		model.addAttribute("pair", pair);
		return "track";
	}
	
	@RequestMapping(value = "admin/tracked", method = RequestMethod.POST)
	public String postTrackedPage(@RequestBody Pair<Long, List<Pair<Long,String>>> pair,ModelMap model){
		
		//packet.setCurrentcity(packet.getSender());
		//packet.setTracking(false);
		
		//packet.setRoute(null);
		ArrayOfPair pairs = new ArrayOfPair();
		List<org.datacontract.schemas._2004._07.admin_service.Pair> pairr = new ArrayList<org.datacontract.schemas._2004._07.admin_service.Pair>();
		
		for(Pair<Long,String> p : pair.getSecond()){
			org.datacontract.schemas._2004._07.admin_service.Pair pp = new org.datacontract.schemas._2004._07.admin_service.Pair();
			pp.setFirst(  p.getFirst().longValue() );
			pp.setSecond(p.getSecond());
			pairr.add(pp);
		}
		pairs.setPair(pairr);
		adminService.trackPacket(pair.getFirst().longValue(),pairs);
		
		return getAdminPage(model);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}
	
}
