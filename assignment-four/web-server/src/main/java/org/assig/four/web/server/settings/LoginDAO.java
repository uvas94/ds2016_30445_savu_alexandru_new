package org.assig.four.web.server.settings;

import java.util.ArrayList;
import java.util.List;

import org.assig.four.web.server.services.PacketService;
import org.assig.four.web.server.services.PacketServices;
import org.assig.four.web.server.services.packetservices.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class LoginDAO  implements UserDetailsService{
	
	private PacketServices packetServices;
	public LoginDAO(){
	}
	
	

	private List<GrantedAuthority> getGrantedAuthorities(User usr) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_" + usr.getRole()));
		return authorities;
	}

	public PacketServices getEmployeeGate() {
		return packetServices;
	}

	public void setEmployeeGate(PacketServices packetServices) {
		this.packetServices = packetServices;
	}

	public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
		packetServices = new PacketService().getPacketServicesImplPort();
		if(packetServices == null)
			throw new UsernameNotFoundException("No db found");
		
		User emp = packetServices.findUserByUsername(arg0);
		
		if (emp.getUsername() == null || emp.getUsername().equals(""))
			throw new UsernameNotFoundException("User not found");
		return new org.springframework.security.core.userdetails.User(emp.getUsername(), emp.getPassword(), true, true,
				true, true, getGrantedAuthorities(emp));
	}

}
