
package org.assig.four.web.server.services.packetservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for packet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="packet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="city1" type="{PacketServices.services.server.web.four.assig.org}city" minOccurs="0"/>
 *         &lt;element name="city2" type="{PacketServices.services.server.web.four.assig.org}city" minOccurs="0"/>
 *         &lt;element name="city3" type="{PacketServices.services.server.web.four.assig.org}city" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idpacket" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="route" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tracking" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="user1" type="{PacketServices.services.server.web.four.assig.org}user" minOccurs="0"/>
 *         &lt;element name="user2" type="{PacketServices.services.server.web.four.assig.org}user" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "packet", propOrder = {
    "city1",
    "city2",
    "city3",
    "description",
    "idpacket",
    "name",
    "route",
    "tracking",
    "user1",
    "user2"
})
public class Packet {

    protected City city1;
    protected City city2;
    protected City city3;
    protected String description;
    protected int idpacket;
    protected String name;
    protected String route;
    protected byte tracking;
    protected User user1;
    protected User user2;

    /**
     * Gets the value of the city1 property.
     * 
     * @return
     *     possible object is
     *     {@link City }
     *     
     */
    public City getCity1() {
        return city1;
    }

    /**
     * Sets the value of the city1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link City }
     *     
     */
    public void setCity1(City value) {
        this.city1 = value;
    }

    /**
     * Gets the value of the city2 property.
     * 
     * @return
     *     possible object is
     *     {@link City }
     *     
     */
    public City getCity2() {
        return city2;
    }

    /**
     * Sets the value of the city2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link City }
     *     
     */
    public void setCity2(City value) {
        this.city2 = value;
    }

    /**
     * Gets the value of the city3 property.
     * 
     * @return
     *     possible object is
     *     {@link City }
     *     
     */
    public City getCity3() {
        return city3;
    }

    /**
     * Sets the value of the city3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link City }
     *     
     */
    public void setCity3(City value) {
        this.city3 = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the idpacket property.
     * 
     */
    public int getIdpacket() {
        return idpacket;
    }

    /**
     * Sets the value of the idpacket property.
     * 
     */
    public void setIdpacket(int value) {
        this.idpacket = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the route property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoute() {
        return route;
    }

    /**
     * Sets the value of the route property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoute(String value) {
        this.route = value;
    }

    /**
     * Gets the value of the tracking property.
     * 
     */
    public byte getTracking() {
        return tracking;
    }

    /**
     * Sets the value of the tracking property.
     * 
     */
    public void setTracking(byte value) {
        this.tracking = value;
    }

    /**
     * Gets the value of the user1 property.
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    public User getUser1() {
        return user1;
    }

    /**
     * Sets the value of the user1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setUser1(User value) {
        this.user1 = value;
    }

    /**
     * Gets the value of the user2 property.
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    public User getUser2() {
        return user2;
    }

    /**
     * Sets the value of the user2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setUser2(User value) {
        this.user2 = value;
    }

}
