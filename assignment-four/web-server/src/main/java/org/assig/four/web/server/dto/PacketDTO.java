package org.assig.four.web.server.dto;


public class PacketDTO {

	protected Long currentcity;
    protected String description;
    protected Long destinationcity;
    protected Long idpacket;
    protected String name;
    protected Long reciver;
    protected String route;
    protected Long sender;
    protected Long sendercity;
    protected Boolean tracking;
    
    
    public PacketDTO(){}
	public PacketDTO(Long currentcity, String description,
			Long destinationcity, Long idpacket, String name, Long reciver,
			String route, Long sender, Long sendercity, Boolean tracking) {
		super();
		this.currentcity = currentcity;
		this.description = description;
		this.destinationcity = destinationcity;
		this.idpacket = idpacket;
		this.name = name;
		this.reciver = reciver;
		this.route = route;
		this.sender = sender;
		this.sendercity = sendercity;
		this.tracking = tracking;
	}
	public Long getCurrentcity() {
		return currentcity;
	}
	public void setCurrentcity(Long currentcity) {
		this.currentcity = currentcity;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getDestinationcity() {
		return destinationcity;
	}
	public void setDestinationcity(Long destinationcity) {
		this.destinationcity = destinationcity;
	}
	public Long getIdpacket() {
		return idpacket;
	}
	public void setIdpacket(Long idpacket) {
		this.idpacket = idpacket;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getReciver() {
		return reciver;
	}
	public void setReciver(Long reciver) {
		this.reciver = reciver;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public Long getSender() {
		return sender;
	}
	public void setSender(Long sender) {
		this.sender = sender;
	}
	public Long getSendercity() {
		return sendercity;
	}
	public void setSendercity(Long sendercity) {
		this.sendercity = sendercity;
	}
	public Boolean getTracking() {
		return tracking;
	}
	public void setTracking(Boolean tracking) {
		this.tracking = tracking;
	}
    
    
    
}
