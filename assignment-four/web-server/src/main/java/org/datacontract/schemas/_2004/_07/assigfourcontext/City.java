
package org.datacontract.schemas._2004._07.assigfourcontext;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for City complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="City">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cityname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idcity" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="lat" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="long_" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "City", propOrder = {
    "cityname",
    "idcity",
    "lat",
    "_long"
})
public class City {

    @XmlElement(required = true, nillable = true)
    protected String cityname;
    protected long idcity;
    protected double lat;
    @XmlElement(name = "long_")
    protected double _long;

    /**
     * Gets the value of the cityname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityname() {
        return cityname;
    }

    /**
     * Sets the value of the cityname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityname(String value) {
        this.cityname = value;
    }

    /**
     * Gets the value of the idcity property.
     * 
     */
    public long getIdcity() {
        return idcity;
    }

    /**
     * Sets the value of the idcity property.
     * 
     */
    public void setIdcity(long value) {
        this.idcity = value;
    }

    /**
     * Gets the value of the lat property.
     * 
     */
    public double getLat() {
        return lat;
    }

    /**
     * Sets the value of the lat property.
     * 
     */
    public void setLat(double value) {
        this.lat = value;
    }

    /**
     * Gets the value of the long property.
     * 
     */
    public double getLong() {
        return _long;
    }

    /**
     * Sets the value of the long property.
     * 
     */
    public void setLong(double value) {
        this._long = value;
    }

}
