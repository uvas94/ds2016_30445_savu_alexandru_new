
package org.datacontract.schemas._2004._07.admin_service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.admin_service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfPair_QNAME = new QName("http://schemas.datacontract.org/2004/07/admin_service", "ArrayOfPair");
    private final static QName _Pair_QNAME = new QName("http://schemas.datacontract.org/2004/07/admin_service", "Pair");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.admin_service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfPair }
     * 
     */
    public ArrayOfPair createArrayOfPair() {
        return new ArrayOfPair();
    }

    /**
     * Create an instance of {@link Pair }
     * 
     */
    public Pair createPair() {
        return new Pair();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPair }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/admin_service", name = "ArrayOfPair")
    public JAXBElement<ArrayOfPair> createArrayOfPair(ArrayOfPair value) {
        return new JAXBElement<ArrayOfPair>(_ArrayOfPair_QNAME, ArrayOfPair.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Pair }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/admin_service", name = "Pair")
    public JAXBElement<Pair> createPair(Pair value) {
        return new JAXBElement<Pair>(_Pair_QNAME, Pair.class, null, value);
    }

}
