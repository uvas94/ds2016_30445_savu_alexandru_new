
package org.datacontract.schemas._2004._07.assigfourcontext;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.assigfourcontext package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Packet_QNAME = new QName("http://schemas.datacontract.org/2004/07/AssigfourContext", "Packet");
    private final static QName _City_QNAME = new QName("http://schemas.datacontract.org/2004/07/AssigfourContext", "City");
    private final static QName _ArrayOfUser_QNAME = new QName("http://schemas.datacontract.org/2004/07/AssigfourContext", "ArrayOfUser");
    private final static QName _ArrayOfPacket_QNAME = new QName("http://schemas.datacontract.org/2004/07/AssigfourContext", "ArrayOfPacket");
    private final static QName _User_QNAME = new QName("http://schemas.datacontract.org/2004/07/AssigfourContext", "User");
    private final static QName _ArrayOfCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/AssigfourContext", "ArrayOfCity");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.assigfourcontext
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfPacket }
     * 
     */
    public ArrayOfPacket createArrayOfPacket() {
        return new ArrayOfPacket();
    }

    /**
     * Create an instance of {@link Packet }
     * 
     */
    public Packet createPacket() {
        return new Packet();
    }

    /**
     * Create an instance of {@link ArrayOfUser }
     * 
     */
    public ArrayOfUser createArrayOfUser() {
        return new ArrayOfUser();
    }

    /**
     * Create an instance of {@link ArrayOfCity }
     * 
     */
    public ArrayOfCity createArrayOfCity() {
        return new ArrayOfCity();
    }

    /**
     * Create an instance of {@link City }
     * 
     */
    public City createCity() {
        return new City();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Packet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/AssigfourContext", name = "Packet")
    public JAXBElement<Packet> createPacket(Packet value) {
        return new JAXBElement<Packet>(_Packet_QNAME, Packet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link City }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/AssigfourContext", name = "City")
    public JAXBElement<City> createCity(City value) {
        return new JAXBElement<City>(_City_QNAME, City.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/AssigfourContext", name = "ArrayOfUser")
    public JAXBElement<ArrayOfUser> createArrayOfUser(ArrayOfUser value) {
        return new JAXBElement<ArrayOfUser>(_ArrayOfUser_QNAME, ArrayOfUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPacket }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/AssigfourContext", name = "ArrayOfPacket")
    public JAXBElement<ArrayOfPacket> createArrayOfPacket(ArrayOfPacket value) {
        return new JAXBElement<ArrayOfPacket>(_ArrayOfPacket_QNAME, ArrayOfPacket.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/AssigfourContext", name = "User")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/AssigfourContext", name = "ArrayOfCity")
    public JAXBElement<ArrayOfCity> createArrayOfCity(ArrayOfCity value) {
        return new JAXBElement<ArrayOfCity>(_ArrayOfCity_QNAME, ArrayOfCity.class, null, value);
    }

}
