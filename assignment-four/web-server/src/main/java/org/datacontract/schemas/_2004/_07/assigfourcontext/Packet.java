
package org.datacontract.schemas._2004._07.assigfourcontext;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Packet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Packet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currentcity" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="destinationcity" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idpacket" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="reciver" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="route" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sender" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="sendercity" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="tracking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Packet", propOrder = {
    "currentcity",
    "description",
    "destinationcity",
    "idpacket",
    "name",
    "reciver",
    "route",
    "sender",
    "sendercity",
    "tracking"
})
public class Packet {

    protected long currentcity;
    @XmlElement(required = true, nillable = true)
    protected String description;
    protected long destinationcity;
    protected long idpacket;
    @XmlElement(required = true, nillable = true)
    protected String name;
    protected long reciver;
    @XmlElement(required = true, nillable = true)
    protected String route;
    protected long sender;
    protected long sendercity;
    protected boolean tracking;

    /**
     * Gets the value of the currentcity property.
     * 
     */
    public long getCurrentcity() {
        return currentcity;
    }

    /**
     * Sets the value of the currentcity property.
     * 
     */
    public void setCurrentcity(long value) {
        this.currentcity = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the destinationcity property.
     * 
     */
    public long getDestinationcity() {
        return destinationcity;
    }

    /**
     * Sets the value of the destinationcity property.
     * 
     */
    public void setDestinationcity(long value) {
        this.destinationcity = value;
    }

    /**
     * Gets the value of the idpacket property.
     * 
     */
    public long getIdpacket() {
        return idpacket;
    }

    /**
     * Sets the value of the idpacket property.
     * 
     */
    public void setIdpacket(long value) {
        this.idpacket = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the reciver property.
     * 
     */
    public long getReciver() {
        return reciver;
    }

    /**
     * Sets the value of the reciver property.
     * 
     */
    public void setReciver(long value) {
        this.reciver = value;
    }

    /**
     * Gets the value of the route property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoute() {
        return route;
    }

    /**
     * Sets the value of the route property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoute(String value) {
        this.route = value;
    }

    /**
     * Gets the value of the sender property.
     * 
     */
    public long getSender() {
        return sender;
    }

    /**
     * Sets the value of the sender property.
     * 
     */
    public void setSender(long value) {
        this.sender = value;
    }

    /**
     * Gets the value of the sendercity property.
     * 
     */
    public long getSendercity() {
        return sendercity;
    }

    /**
     * Sets the value of the sendercity property.
     * 
     */
    public void setSendercity(long value) {
        this.sendercity = value;
    }

    /**
     * Gets the value of the tracking property.
     * 
     */
    public boolean isTracking() {
        return tracking;
    }

    /**
     * Sets the value of the tracking property.
     * 
     */
    public void setTracking(boolean value) {
        this.tracking = value;
    }

}
