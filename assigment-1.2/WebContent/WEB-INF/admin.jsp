<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Flights</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Resources/AdminStylesheet.css"/>
		
		<script>
		var newflightId;
		var addUrl = "${pageContext.request.contextPath}/admin/addFlight";
		var deleteUrl = "${pageContext.request.contextPath}/admin/deleteFlight";
		var updateUrl = "${pageContext.request.contextPath}/admin/updateFlight";
		var logoutUrl = "${pageContext.request.contextPath}/login"
		
		function setUpdateForm(element){
			
			trElement = document.getElementById(element);
			//changeSelectValue(document.flightForm.role, trElement.children[2].innerHTML);
			
			document.flightForm.id.value = trElement.children[0].innerHTML;
			document.flightForm.type.value = trElement.children[1].innerHTML;
			
			 selectedOption(trElement.children[2].innerHTML,document.flightForm.departureCity);
			//document.flightForm.departureCity.selectIndex = selectedOption;
			document.flightForm.departureDate.value = trElement.children[3].innerHTML;
			
			selectedOption(trElement.children[4].innerHTML,document.flightForm.arrivalCity);
			//document.flightForm.arrivalCity.selectIndex = selectedOption(trElement.children[4].innerHTML,document.flightForm.arrivalCity);
			document.flightForm.arrivalDate.value = trElement.children[5].innerHTML;
			document.flightForm.methodd.value = "update";
			
			
			document.flightForm.newflight.disabled = false;
			document.flightForm.deleteflight.disabled = false;
			document.flightForm.action = updateUrl;
		}

		function selectedOption(city,element){
			
			var i = 0; 
			var options =  document.getElementById(element.id).options;
			for( i = 0 ; i <= options.length; i++)
				if(options[i].text == city)
				{	options[i].selected = true;
				return true;
				}
			return false;
		}
	
		function setAddForm(){
			document.flightForm.id.value = "";
			document.flightForm.type.value = "";
			document.getElementById("departureCity").selectedIndex = 0;
			
			document.getElementById("arrivalCity").selectedIndex = 0;
			
			document.flightForm.name.readOnly = false;
			document.flightForm.newflight.disabled = true;
			document.flightForm.deleteflight.disabled = true;
			document.flightForm.action =  addUrl;
			document.flightForm.methodd.value = "add";
		}

		function setDeleteForm() {
			document.flightForm.method.value = "delete";
			document.flightForm.action =   deleteUrl;
		}
		function setLogoutForm() {
			document.flightForm.methodd.value = "logout";
			document.flightForm.action =   logoutUrl;
			document.flightForm.method = "get";
		}
		</script>
		
	</head>
	<body>
		<fieldset>
		<legend>Flight Form</legend>
		<center>
		<form  action="${pageContext.request.contextPath}/admin/addFlight" name="flightForm" method="post">
		<table>
			<tr><td><c:out value ="${requestScope.msg}"/> </td></tr>
			<tr>	 <input type="hidden" id = "methodd" name="methodd" value="add">	</tr>
			<tr>	<td>Flight Id</td>		<td> <input id ="fligtId" name = "id" type = "text"/></td>			</tr>
			<tr>	<td>Airplane Type </td>	<td> <input id = "airplaneType" name = "type" type = "text"/></td>	</tr>
			<tr>	<td>Departure City</td>	<td> <select id = "departureCity" name = "departureCity">
												<c:if test="${!empty cities}">
														<c:forEach items="${cities}" var="city">
															<option id = "${city.id}"><c:out value="${city.cityName}"/></option>
														</c:forEach>
													</c:if>
												</select></td>
												</tr>
			<tr>	<td>Departure Date</td>	<td> <input id = "departureDate" name = "departureDate" type = "text" placeholder = "yyyy-mm-dd"/></td>	</tr>
			<tr>	<td>Arrival City</td>	<td> <select id = "arrivalCity" name = "arrivalCity">
													<c:if test="${!empty cities}">
														<c:forEach items="${cities}" var="city">
															<option id = "${city.id}"><c:out value="${city.cityName}"/></option>
														</c:forEach>
													</c:if>
												</select></td>	</tr>
			<tr>	<td>Arrival Date</td>	<td> <input id = "arrivalDate" name = "arrivalDate" type = "text"  placeholder = "yyyy-mm-dd"/></td>	</tr>
			<tr><td colspan="2">
			<input type="submit" value="Save Changes"/>
			&nbsp;<input type="reset" name="newflight" value="New flight" onclick="setAddForm();" disabled="disabled"/>
			&nbsp;<input type="submit" name="deleteflight" value="Delete flight" onclick="setDeleteForm();" disabled="disabled"/>
			&nbsp;<input type="submit" name="logout" value="logout" onclick="setLogoutForm();"/></td></tr>
			
		</table>
		</form>
		</center>
		</fieldset>
		<c:if test="${!empty flights}">
		<fieldset >
			<br />
			<center>
				<table width="90%">
					<tr style="background-color: #093145;">
						<th>Flight id</th>
						<th>Airplane Type</th>
						<th>Departure city</th>
						<th>Departure date</th>
						<th>Arrival city</th>
						<th>Arrival date</th>
					</tr>
					<c:forEach items="${flights}" var="flight">
						<tr style="background-color: #107896;" id="f${flight.id}" onclick="setUpdateForm('f${flight.id}');">
							<td><c:out value="${flight.id}"/></td>
							<td><c:out value="${flight.airplaneType}"/></td>
							<td><c:out value="${flight.departureCity.cityName}"/></td>
							<td><c:out value="${flight.departureDate}"/></td>
							<td><c:out value="${flight.arrivalCity.cityName}"/></td>
							<td><c:out value="${flight.arrivalDate}"/></td>
						</tr>
					</c:forEach>
				</table>
				</center>
			<br />
		</fieldset>
		</c:if>
	</body>
</html>