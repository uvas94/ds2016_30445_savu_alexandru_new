<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Flights</title>
		
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Resources/AdminStylesheet.css"/>
		
		<script>
		var updateUrl = "${pageContext.request.contextPath}/user/getLocalTime";
		var logoutUrl = "${pageContext.request.contextPath}/login"
		
		function setUpdateForm(element){
			
			trElement = document.getElementById(element);
			//changeSelectValue(document.cityForm.role, trElement.children[2].innerHTML);
		
			 document.cityForm.departureCity.value = trElement.children[2].innerHTML;
			
			 document.cityForm.arrivalCity.value = trElement.children[4].innerHTML;
			
			
			/* document.cityForm.city1Time.disabled = false;
			document.cityForm.city2Time.disabled = false; */
			document.cityForm.cityTime.disabled = false;
			document.cityForm.action = updateUrl;
		}

		function setCity1Time(){
			document.cityForm.methodd = "departureCityTime";
		}
		function setCity2Time(){
			document.cityForm.methodd = "arrivalCityTime";
			
		}

		function setLogoutForm() {
			//document.cityForm.methodd.value = "logout";
			document.cityForm.action =   logoutUrl;
			document.cityForm.method = "get";
		}
		</script>
		
	</head>
	<body>
		<fieldset>
		<legend>Flight Form</legend>
		<center>
		<form  action="${pageContext.request.contextPath}/user/getLocalTime" name="cityForm" method="post">
		<table>
			<tr><td><c:out value ="${requestScope.msg}"/> </td></tr>
			<!--	<tr>	 <input type="hidden" id = "methodd" name="methodd" value="departureCityTime">	</tr>	-->
			<tr>	<td>Departure City</td>	<td> <input id = "departureCity" name = "departure" type = "text" readonly="readonly"/> </td> 
			<td>Local Time: ${departureCityTime}</td>									
			</tr>
			
			<tr>	<td>Arrival City</td>	<td> <input id = "arrivalCity" name = "arrival" type = "text" readonly="readonly"/></td>	
		
			<td>Local Time: ${arrivalCityTime}</td>
			</tr>
			<tr><td colspan="1">
			<input type="submit" name="cityTime" value="Get local time" disabled="disabled"/>
			&nbsp;<input type="submit" name="logout" value="logout" onclick="setLogoutForm();"/></td></tr>
			
		</table>
		</form>
		</center>
		</fieldset>
		<c:if test="${!empty flights}">
		<fieldset >
			<br />
			<center>
				<table width="90%">
					<tr style="background-color: #093145;">
						<th>Flight id</th>
						<th>Airplane Type</th>
						<th>Departure city</th>
						<th>Departure date</th>
						<th>Arrival city</th>
						<th>Arrival date</th>
					</tr>
					<c:forEach items="${flights}" var="flight">
						<tr style="background-color: #107896;" id="f${flight.id}" onclick="setUpdateForm('f${flight.id}');">
							<td><c:out value="${flight.id}"/></td>
							<td><c:out value="${flight.airplaneType}"/></td>
							<td><c:out value="${flight.departureCity.cityName}"/></td>
							<td><c:out value="${flight.departureDate}"/></td>
							<td><c:out value="${flight.arrivalCity.cityName}"/></td>
							<td><c:out value="${flight.arrivalDate}"/></td>
						</tr>
					</c:forEach>
				</table>
				</center>
			<br />
		</fieldset>
		</c:if>
	</body>
</html>