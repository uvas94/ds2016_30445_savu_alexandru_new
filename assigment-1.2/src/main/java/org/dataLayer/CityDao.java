package org.dataLayer;

import java.util.List;

import org.entities.City;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class CityDao extends AbstractDao<Integer,City>{

	public City findCitytById(int id) {
		return getByKey(id);
	}

	
	public boolean saveCity(City city) {
			return persist(city);
		
	}

	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<City> findAllCity() {
		
  
        Session session = getSession();
		Transaction tx = null;
		List<City> cities = null;
		try {
			tx = session.beginTransaction();
			cities = session.createQuery("FROM City").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return cities;
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<City> findCityByName(String name) {
	     Session session = getSession();
			Transaction tx = null;
			List<City> cities = null;
			try {
				tx = session.beginTransaction();
				Query<City> query =  session.createQuery("FROM City WHERE name =:name");
				query.setParameter("name", name);
				cities = query.list();
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
			} finally {
				session.close();
			}
			return cities;
		
	}
	
	
	
}
