package org.presentationLayer;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bussinesLayer.FlightService;
import org.entities.City;
import org.entities.Flight;

public class AdminServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3800473900293728062L;

	private FlightService flightService;
	
	public void init(){
		flightService = new FlightService();
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		//String username = (String) session.getAttribute("userId");
		String role = (String) session.getAttribute("role");
		String url = null;
		if(role == null || role.equals("User")){
			request.setAttribute("error", "You don't have the authority to access that page");
			 url = "/WEB-INF/login.jsp";
			  }
		else
		{
			try{
			
			ArrayList< Flight> flights = (ArrayList<Flight>) flightService.getAllFlight();
			request.setAttribute("flights", flights);
			
			ArrayList<City> cities = (ArrayList<City>) flightService.getAllCities();
			request.setAttribute("cities", cities);
			
			
			}
			catch(Exception e){
				System.out.println("Error"+e);
				
			}
			url = "/WEB-INF/admin.jsp";
			
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
	//add update delete
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		String role = (String) session.getAttribute("role");
		String url = null;
		if(role == null || role.equals("User")){
				request.setAttribute("error", "You don't have the authority to access that page");
		 		url = "/WEB-INF/login.jsp";
		 	}
		else
		{
			
			try{
				
				
				
				String idString = (String) request.getParameter("id");
				String airplaneType = (String) request.getParameter("type");
				String departureCity = (String)  request.getParameter("departureCity");
				String deparureDateString  = (String) request.getParameter("departureDate");
				String arrivalCity = (String)  request.getParameter("arrivalCity");
				String arrivalDateString  = (String) request.getParameter("arrivalDate");
				String method = (String) request.getParameter("methodd");
				
				int id = Integer.parseInt(idString);
				DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
				Date arrivalDate = new Date( ( (java.util.Date)df.parse(arrivalDateString) ).getTime() );
				Date deparDate = new Date( ( (java.util.Date)df.parse(deparureDateString) ).getTime() );;
				
				
					if(method.equals("add"))
					{if(!flightService.saveFlight(id,airplaneType,departureCity,deparDate,arrivalCity,arrivalDate))
						request.setAttribute("msg", "Operation Faild");}
					else if(method.equals("update"))
						{if(!flightService.updateFlight(id,airplaneType,departureCity,deparDate,arrivalCity,arrivalDate))
							request.setAttribute("msg", "Operation Faild");}
						
					else if(method.equals("delete"))
						{if(!flightService.deleteFlight(id))
							request.setAttribute("msg", "Operation Faild");
						}
					else
						request.setAttribute("msg", "Operation Faild");
									
			}
			catch(Exception e){
				request.setAttribute("msg", "Operation Faild");
				System.out.println("Error : "+e);
			}
			
			ArrayList< Flight> flights = (ArrayList<Flight>) flightService.getAllFlight();
			request.setAttribute("flights", flights);
			
			ArrayList<City> cities = (ArrayList<City>) flightService.getAllCities();
			request.setAttribute("cities", cities);
			
			url = "/WEB-INF/admin.jsp";
		}
		request.getRequestDispatcher(url).forward(request, response);
		
	}
	
}
