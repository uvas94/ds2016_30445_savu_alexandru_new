package org.bussinesLayer;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.dataLayer.CityDao;
import org.dataLayer.FlightDoa;
import org.entities.City;
//import org.entities.City;
import org.entities.Flight;

public class FlightService {

	FlightDoa flightdao ;
	CityDao ctDao;
	public FlightService() {
		flightdao = new FlightDoa();
		ctDao = new CityDao();
	}
	
	public List<City> getAllCities(){
		return ctDao.findAllCity();
	}
	
	public List<Flight> getAllFlight(){
		return flightdao.findAllFlights();
	}
	
	public boolean updateFlight(int id, String airplaneType, String departureCity,
			Date deparDate, String arrivalCity, Date arrivalDate){
		
		Flight flight1 = flightdao.getByKey(id);
		if(flight1 != null)
		{
			return saveFlight(id ,airplaneType, departureCity,deparDate,arrivalCity,arrivalDate);
		}
			
		return false;
	}
	

	
	public boolean deleteFlight(int id){
		
		return flightdao.deleteFlightById(id);
	}

	public boolean saveFlight(int id, String airplaneType, String departureCity,
			Date deparDate, String arrivalCity, Date arrivalDate) {
		
		if(arrivalCity.equals(departureCity))
			return false;
		
		if(deparDate.after(arrivalDate))
			return false;
		City arrvcity = ctDao.findCityByName(arrivalCity).get(0);
		City departcity = ctDao.findCityByName(departureCity).get(0);
		
		
		Flight flight = new Flight(id,airplaneType,departcity,deparDate,arrvcity,arrivalDate);
		
		return flightdao.saveFlight(flight);
		
	}

	public ArrayList<Double> cityCoordinates(String city){
		City city1 = ctDao.findCityByName(city).get(0);
		//first lat second long
		ArrayList<Double> coor = new ArrayList<Double>();
		coor.add(city1.getLatitude());
		coor.add(city1.getLongitude());
		return coor;
	}
	
}
