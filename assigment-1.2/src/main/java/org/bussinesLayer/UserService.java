package org.bussinesLayer;

import java.util.List;

import org.dataLayer.UserDao;
import org.entities.User;

public class UserService {
	 UserDao  userDao;
	 
	 public UserService() {
		userDao = new UserDao();
	}
	 public boolean verifyUser(String username, String password){
		 
		 List<User> userList = userDao.findUserByName(username);
		 
		 if(userList.size() == 0)
			 return false;
		 User usr = userList.get(0);
			
		 if(!usr.getPassword().equals(password))
		 return false;
		
		 return true;
	 }
	 public String getUserRole(String username){
		 
		 List<User> userList = userDao.findUserByName(username);
		 
		 if(userList.size() == 0)
		 return null;
		 
		 return userList.get(0).getRole();
	 }
}
