package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.apache.commons.codec.binary.Base64InputStream;
import org.common.Dvd;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Consumer Client application. This application
 *  will run in an infinite loop and retrieve messages from the queue server
 *  and send e-mails with them as they come.
 */
public class ClientStart {

	private ClientStart() {
	}

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost",8888);

		MailService mailService = new MailService("uvas94@gmail.com","!gZaZlZ_321!");
		String message;

		while(true) {
			try {
				message = queue.readMessage();
				
				ObjectInputStream ios  = (ObjectInputStream) new ObjectInputStream(new Base64InputStream( 
						new ByteArrayInputStream(message.getBytes())));
					//ObjectInputStream ios = new ObjectInputStream(  new ByteArrayInputStream(message.getBytes()));
			      Dvd dvd = (Dvd) ios.readObject();
			      ios.close();
				
				System.out.println("Sending mail "+dvd.toString());
				mailService.sendMail("uvas94@yahoo.com","Dummy Mail Title",message);
				mailService.sendMail("uvas955@yahoo.com","Dummy Mail Title",message);
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
