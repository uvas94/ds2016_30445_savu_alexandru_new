package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.apache.commons.codec.binary.Base64InputStream;
import org.common.DVDtoXML;
import org.common.Dvd;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;

public class ClientStartWriteData {

	private ClientStartWriteData() {
	}

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost",8888);

		
		String message;

		while(true) {
			try {
				message = queue.readMessage();
				
				
				ObjectInputStream ios  = (ObjectInputStream) new ObjectInputStream(new Base64InputStream( 
						new ByteArrayInputStream(message.getBytes())));
					//ObjectInputStream ios = new ObjectInputStream(  new ByteArrayInputStream(message.getBytes()));
			      Dvd dvd = (Dvd) ios.readObject();
			      ios.close();
			    System.out.println("Write data for dvd: " +  dvd.toString());
				DVDtoXML.jaxbObjectToXML(dvd);
				
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
}
