package org.common;
	import java.io.File;

	import javax.xml.bind.JAXBContext;
	import javax.xml.bind.Marshaller;

	public class DVDtoXML {

		//private static final String FINAL_NAME = "book.xml";

		

		public static void jaxbObjectToXML(Dvd bck) {

			try {
				JAXBContext context = JAXBContext.newInstance(Dvd.class);
				Marshaller m = context.createMarshaller();
				// for pretty-print XML in JAXB
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

				//Write to System.out for debugging
				// m.marshal(bck, System.out);

				// Write to File
				//m.marshal(bck, new ClassPathResource(FINAL_NAME).getFile());
				String filename = bck.getTitle()+".xml";
				m.marshal(bck, new File(filename));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


