package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import ro.tuc.dsrl.ds.handson.assig.three.producer.connection.QueueServerConnection;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.apache.commons.codec.binary.Base64OutputStream;
import org.common.Dvd;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Producer Client application. This
 *	application will send several messages to be inserted
 *  in the queue server (i.e. to be sent via email by a consumer).
 */
public class ClientStart {
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection(HOST, PORT);

		try {
			for (int i=0;i<5;i++) {
				
				Dvd dvd = new Dvd("Film"+i,2000,200.0);
				
				 
				      ByteArrayOutputStream baos = new ByteArrayOutputStream();
				      ObjectOutputStream oos = new ObjectOutputStream(
				              new Base64OutputStream(baos));
				      oos.writeObject(dvd);
				      oos.close();
				    
				    
				
				queue.writeMessage(baos.toString("UTF-8"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
