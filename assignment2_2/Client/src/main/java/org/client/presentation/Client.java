package org.client.presentation;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;


import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.common.Car;
import org.common.ICarValueCalculator;

public class Client extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	ICarValueCalculator calculator;
	
	@Override
	public void init() throws ServletException {
		try {
			calculator = (ICarValueCalculator) Naming.lookup("rmi://localhost:5000/calculator");
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.init();
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.getRequestDispatcher("/WEB-INF/client.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		try{
			
			
			
			String yearString = (String) request.getParameter("year");
			String engineSizeString = (String) request.getParameter("engineSize");
			String priceString = (String)  request.getParameter("price");
			String method = (String) request.getParameter("methodd");
			
			int year = Integer.parseInt(yearString);
			int engineSize = Integer.parseInt(engineSizeString);
			int price = Integer.parseInt(priceString);
			Car car = new Car(engineSize, price, year);
			NumberFormat formatter = new DecimalFormat("#0.00");   
				if(method.equals("Tax"))
				{
					request.setAttribute("tax","The tax is : "+formatter.format(calculator.computeTax(car)));
				}
				else if(method.equals("Price"))
					request.setAttribute("newPrice","Current value is :"+formatter.format(calculator.computePrice(car)));
				else
					request.setAttribute("msg", "Operation Faild");
					
								
		}
		catch(Exception e){
			request.setAttribute("msg", "Operation Faild");
			System.out.println("Error : "+e);
		}
		
		request.getRequestDispatcher("/WEB-INF/client.jsp").forward(request, response);
	}
	
}
