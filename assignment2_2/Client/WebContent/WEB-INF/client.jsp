<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Client</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Resources/ClientStylesheet.css"/>
		
		<script>
			function verfication(btn)
			{
				var engineSizeString = document.getElementById("engineSize");
				var yearString = document.getElementById("year");
				var priceString = document.getElementById("price");
				var taxBtn = document.getElementById("taxBtn");
				var priceBtn = document.getElementById("priceBtn");
				var methodd = document.getElementById("methodd");
				var submit = true;
				
				var engineSize = parseInt(engineSizeString.value);
				var year = parseInt(yearString.value);
				var price = parseInt(priceString.value);
				
				if( isNaN(engineSize)  || engineSize < 0)
					{
					engineSizeString.style.background = "red";
					submit = false;
					}
				else
					{
						engineSizeString.removeAttribute("style");
					}
				if( isNaN(year) || year < 0)
					{
						yearString.style.background = "red";
						submit = false;
					}
				else
					{
						yearString.removeAttribute("style");
					}
				if( isNaN(price) || price < 0)
					{
						priceString.style.background = "red";
						submit = false;
					}
				else
					{
						priceString.removeAttribute("style");
					}
				
				if(submit)
					{
					taxBtn.type = "submit";
					priceBtn.type = "submit";
					}
				else
					{
						taxBtn.type = "button";
						priceBtn.type = "button";
					}
				if(btn == 1){
					
					methodd.value = "Tax";
					}
				else
					methodd.value = "Price";
				
			}
		</script>
</head>
<body>
	
	<form  action="${pageContext.request.contextPath}/client" method = "post" >
	
			<table>
			<tr><td colspan="2"><c:out value ="${requestScope.msg}"/> </td></tr>
			<tr><td> <input type="hidden" id = "methodd" name="methodd" value="Tax"><td> </tr>
			<tr>
			<td><label for = "engineSize">Engine Size </label></td>
			<td><input id = "engineSize" name = "engineSize" type ="text"/></td> 
			</tr>
			<tr>
			<td>
			<label for = "year"> Year </label> </td> 
			<td><input id = "year" name = "year" type = "text"/> </td> 
			</tr>
			<tr>
			<td>
			<label for = "price"> Price </label> </td> 
			<td><input id = "price"  name = "price" type = "text"/> 
			</td> 
			</tr>
			<tr><td colspan="2">
			<input id = "taxBtn" type = "button" value = "Tax" onclick ="verfication(1)"/>
			<input id = "priceBtn" type= "button" value = "Current Value" onclick ="verfication(2)"/>
			</td>
			</tr>
			<tr><td><c:out value ="${requestScope.tax}"/> </td></tr>
			<tr><td colspan = "2"><c:out value ="${requestScope.newPrice}"/> </td></tr>
			</table>
	</form>
</body>
</html>