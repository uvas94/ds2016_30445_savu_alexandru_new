package org.services;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.common.*;

public class CarValueCalculator extends UnicastRemoteObject implements ICarValueCalculator {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CarValueCalculator() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	public double computeTax(Car c) {
	
		int capacity = c.getEngineSize();
		if (capacity <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if(capacity > 1601) sum = 18;
		if(capacity > 2001) sum = 72;
		if(capacity > 2601) sum = 144;
		if(capacity > 3001) sum = 290;
		return capacity / 200.0 * sum;
		
	}

	public double computePrice(Car c) {
		double price = 0;
		double initialPrice = c.getPrice();
		price = initialPrice - initialPrice / 7 * (2015 - c.getYear());
		
		if(price < 0)
			return 0;
		
		return price;
	}
		
		
}
