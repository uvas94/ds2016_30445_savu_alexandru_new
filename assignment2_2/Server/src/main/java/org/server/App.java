package org.server;

import java.rmi.*;
import java.rmi.registry.LocateRegistry;

import org.common.ICarValueCalculator;
import org.services.CarValueCalculator;

public class App 
{
    public static void main( String[] args )
    {
        try{
        	System.out.println("--------------------Starting connection-----------------------");
        	ICarValueCalculator stub = new CarValueCalculator();
        	
        	LocateRegistry.createRegistry(5000);
        	Naming.rebind("rmi://localhost:5000/calculator", stub);
        	
        	System.out.println("--------------------Connection started-----------------------");
        }
        catch(Exception e){
        	System.out.println("Main error : " + e);
        	
        }
    }
}
