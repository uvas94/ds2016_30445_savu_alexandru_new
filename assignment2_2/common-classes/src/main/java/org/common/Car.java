package org.common;

import java.io.Serializable;

public class Car implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int engineSize;
	private int Price;
	private int year;
	
	public Car(int engineSize, int price, int year) {
		super();
		this.engineSize = engineSize;
		Price = price;
		this.year = year;
	}
	
	public int getEngineSize() {
		return engineSize;
	}
	public void setEngineSize(int engineSize) {
		this.engineSize = engineSize;
	}
	public int getPrice() {
		return Price;
	}
	public void setPrice(int price) {
		Price = price;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}

	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (Price != other.Price)
			return false;
		if (engineSize != other.engineSize)
			return false;
		if (year != other.year)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Car [engineSize=" + engineSize + ", Price=" + Price + ", year="
				+ year + "]";
	}
	
	
	
}
