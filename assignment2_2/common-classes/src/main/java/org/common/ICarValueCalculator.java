package org.common;

import java.rmi.*;
public interface ICarValueCalculator extends Remote{
	
		public double computeTax(Car c)  throws RemoteException;
		public double computePrice(Car c)  throws RemoteException;
}
