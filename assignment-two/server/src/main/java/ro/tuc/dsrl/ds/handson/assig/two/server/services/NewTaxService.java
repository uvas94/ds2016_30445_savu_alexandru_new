package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

public class NewTaxService {

	public double computeTax(Car c) {
		// Dummy formula
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		double price = 0;
		double initialPrice =  c.getInitialPrice();
		price = initialPrice;
		price -=  initialPrice / 7 * (2015 - c.getYear());
		return price;
	}
}
