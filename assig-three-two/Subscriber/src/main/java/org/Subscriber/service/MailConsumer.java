package org.Subscriber.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.Commun.DVD;
import org.apache.commons.codec.binary.Base64InputStream;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class MailConsumer extends DefaultConsumer{

	public MailConsumer(Channel channel) {
		super(channel);
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("resource")
	@Override
    public void handleDelivery(String consumerTag, Envelope envelope,
                               AMQP.BasicProperties properties, byte[] body) throws IOException {
      String message = new String(body, "UTF-8");
      System.out.println(" [x] Received '" + message + "'");
      ObjectInputStream ios  = (ObjectInputStream) new ObjectInputStream(new Base64InputStream( 
				new ByteArrayInputStream(message.getBytes())));
			//ObjectInputStream ios = new ObjectInputStream(  new ByteArrayInputStream(message.getBytes()));
	      DVD dvd;
		
			
			SAXBuilder ss = new SAXBuilder();
			try {
				dvd = (DVD) ios.readObject();
				Document settings = ss.build(new File("settings.xml"));
				
				Element rootElement = settings.getRootElement();
				
				String username = rootElement.getChildText("username");
				String password = rootElement.getChildText("password");
				
				MailService mail = new MailService(username, password);
				
				String mailMessage = "The movie "+dvd.getTitle()+" from "+ dvd.getYear()+" was added to the store.\n"
								+"You can buy it for only " + dvd.getPrice() + " dollars.";
				mail.sendMail("uvas94@yahoo.com", "New dvd", mailMessage);
				mail.sendMail("uvas955@yahoo.com", "New dvd", mailMessage);
			} catch (JDOMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
	      ios.close();
	   
    }

}
