package org.Subscriber.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.Commun.DVD;
import org.Commun.DVDtoXML;
import org.apache.commons.codec.binary.Base64InputStream;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class FileConsumer extends DefaultConsumer{
	
	public FileConsumer(Channel channel){
		super(channel);
	}
	
	@SuppressWarnings("resource")
	@Override
    public void handleDelivery(String consumerTag, Envelope envelope,
                               AMQP.BasicProperties properties, byte[] body) throws IOException {
      String message = new String(body, "UTF-8");
      System.out.println(" [x] Received '" + message + "'");
      ObjectInputStream ios  = (ObjectInputStream) new ObjectInputStream(new Base64InputStream( 
				new ByteArrayInputStream(message.getBytes())));
			//ObjectInputStream ios = new ObjectInputStream(  new ByteArrayInputStream(message.getBytes()));
	      DVD dvd;
		try {
			dvd = (DVD) ios.readObject();
			 System.out.println("Write data for dvd: " +  dvd.toString());
				DVDtoXML.jaxbObjectToXML(dvd);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      ios.close();
	   
    }
  

}
