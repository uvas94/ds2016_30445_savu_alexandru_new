package org.Subscriber.start;

import com.rabbitmq.client.*;

public class BaseSubscriber  {
  private static final String EXCHANGE_NAME = "dvd";
  //org.Subscriber.service.MailConsumer
  //org.Subscriber.service.FileConsumer
  @SuppressWarnings("unchecked")
public static void main(String[] argv) throws Exception {
	  
	if(argv.length != 1){
		System.out.println("Not enough arguments " + argv.length +"\n");
		return;
	}  
	System.out.println("-----------------------------Before opening the connection-------------------------------");
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    Connection connection = factory.newConnection();
    System.out.println("-----------------------------Before opening the channel-------------------------------");
    Channel channel = connection.createChannel();
    System.out.println("-----------------------------After opening the channel----------------------------------");
    
    System.out.println("-----------------------------Queue-----------------------------------");
    channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
    String queueName = channel.queueDeclare().getQueue();
    channel.queueBind(queueName, EXCHANGE_NAME, "");
    
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
  
    
    //System.out.println(argv[0]);
    Class<Consumer> c = (Class<Consumer>) Class.forName(argv[0]);
    
   
    Consumer consumer = (Consumer) c.getConstructor(Channel.class).newInstance(channel);

    if(consumer == null)
    	 System.out.println("-----------------------------Faild Class-----------------------------------");
    System.out.println("-----------------------------Classs-----------------------------------");
    channel.basicConsume(queueName, true, consumer);
  }
 /*
 public void consumer(Consumer consumer) throws IOException, TimeoutException{
	 
	    ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost("localhost");
	    Connection connection = factory.newConnection();
	    
	    Channel channel = connection.createChannel();
	   
	    channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
	    String queueName = channel.queueDeclare().getQueue();
	    channel.queueBind(queueName, EXCHANGE_NAME, "");
	    
	    System.out.println(" [*] Waiting for messages.");
	    
	    channel.basicConsume(queueName, true, consumer);
	 
 } */
}