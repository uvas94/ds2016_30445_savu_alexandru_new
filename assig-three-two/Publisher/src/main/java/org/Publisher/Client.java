package org.Publisher;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.Commun.DVD;
import org.apache.commons.codec.binary.Base64OutputStream;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;


public class Client extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String EXCHANGE_NAME = "dvd";
	
	@Override
	public void init() throws ServletException {
	
		super.init();
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.getRequestDispatcher("/WEB-INF/client.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		try{
			
			
			String title = (String) request.getParameter("title");
			String yearString = (String) request.getParameter("year");
			String priceString = (String)  request.getParameter("price");
			
			int year = Integer.parseInt(yearString);
			int price = Integer.parseInt(priceString);
			
			DVD dvd = new DVD(title,year,price);
			//NumberFormat formatter = new DecimalFormat("#0.00");   
			ConnectionFactory factory = new ConnectionFactory();
	        factory.setHost("localhost");
	        Connection connection = factory.newConnection();
	        Channel channel = connection.createChannel();

	        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
		      ObjectOutputStream oos = new ObjectOutputStream(
		              new Base64OutputStream(baos));
		      oos.writeObject(dvd);
		      oos.close();
		    	

	        channel.basicPublish(EXCHANGE_NAME, "", null, baos.toString("UTF-8").getBytes());
	        System.out.println(" [x] Sent '" + baos.toString("UTF-8") + "'");

	        channel.close();
	        connection.close();
					
								
		}
		catch(Exception e){
			request.setAttribute("msg", "Operation Faild");
			System.out.println("Error : "+e);
		}
		
		request.getRequestDispatcher("/WEB-INF/client.jsp").forward(request, response);
	}
	
}
