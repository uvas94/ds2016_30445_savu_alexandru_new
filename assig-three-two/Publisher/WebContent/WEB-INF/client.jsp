<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Client</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Resources/ClientStylesheet.css"/>
		
		<script>
			function verfication(btn)
			{
				var titleString = document.getElementById("title");
				var yearString = document.getElementById("year");
				var priceString = document.getElementById("price");
				var publishBtn = document.getElementById("publishBtn");
				
				var submit = true;
				
				var title = titleString.value;
				var year = parseInt(yearString.value);
				var price = parseInt(priceString.value);
				
				if( title.lenght > 0 )
					{
					titleString.style.background = "red";
					submit = false;
					}
				else
					{
						titleString.removeAttribute("style");
					}
				if( isNaN(year) || year < 0)
					{
						yearString.style.background = "red";
						submit = false;
					}
				else
					{
						yearString.removeAttribute("style");
					}
				if( isNaN(price) || price < 0)
					{
						priceString.style.background = "red";
						submit = false;
					}
				else
					{
						priceString.removeAttribute("style");
					}
				
				if(submit)
					{
					publishBtn.type = "submit";
					
					}
				else
					{
					publishBtn.type = "button";
					}
				
				
			}
		</script>
</head>
<body>
	
	<form  action="${pageContext.request.contextPath}/client" method = "post" >
	
			<table>
			<tr><td colspan="2"><c:out value ="${requestScope.msg}"/> </td></tr>
			<tr>
			<td><label for = "title">Title </label></td>
			<td><input id = "title" name = "title" type ="text"/></td> 
			</tr>
			<tr>
			<td>
			<label for = "year"> Year </label> </td> 
			<td><input id = "year" name = "year" type = "text"/> </td> 
			</tr>
			<tr>
			<td>
			<label for = "price"> Price </label> </td> 
			<td><input id = "price"  name = "price" type = "text"/> 
			</td> 
			</tr>
			<tr><td colspan="2">
			<input id = "publishBtn" type = "button" value = "Update" onclick ="verfication(1)"/>
			</td>
			</tr>
			
			</table>
	</form>
</body>
</html>